// importing atom from recoil to create a global state 
import { atom } from 'recoil';
import { selector } from 'recoil';
import { recoilPersist } from 'recoil-persist';
import axios from './config.js';
const { persistAtom } = recoilPersist();

// declaring and exporting global state var with key being it's name and default being initial value, it would be updated later to keep the current value
export const cartState = atom({
  key: 'cartState',
  default: [],
  effects_UNSTABLE: [persistAtom],
});

export const wishListState = atom({
  key: 'wishList',
  default: [],
  effects_UNSTABLE: [persistAtom],
});

const productDB = selector({
  key: "productDB",
  get: async ({ get }) => {
    const { data } = await axios.get('/products/');
    console.log(data);
    return data;
  },
});
export const productDBState = atom({
  key: "productDBState",
  default: productDB,
});

export const productDBModifyVersion = selector({
  key: 'productDBModify',
  get: ({get}) => get(productDBState),
  set: ({set}, newValue) => set(productDBState, newValue),
});

export const themeColorState = atom({
  key: 'themeColor',
  default: '',
  effects_UNSTABLE: [persistAtom],
});
export const shippingInfoState = atom({
  key: 'paymentInfo',
  default: {},
  effects_UNSTABLE: [persistAtom],
});
export const orderState = atom({
  key:'order',
  default:[],
  effects_UNSTABLE: [persistAtom],
})
export const userState = atom({
  key:'userInfo',
  default:[],
  effects_UNSTABLE: [persistAtom],
})