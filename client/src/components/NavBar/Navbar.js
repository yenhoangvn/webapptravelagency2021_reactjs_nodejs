import React, {useState} from 'react';
import arrow from './image/download.webp'
import { NavLink } from 'react-router-dom';

function Navbar({isLoggedIn,userName}){
    const [style, setStyle] = useState({display:'none'});
    const [className, setClassName] = useState();
    const handleClick = () =>{
        if(style.display==='none'){
            setStyle({display:'grid'})
            setClassName('visible');
        }else{
            setStyle({display:'none'})
            setClassName('');
        }
    }

    return(
        <div className="top-bar-wrapper">
            <div style={style} className="side-menu-hidden">
                <h1>N'bie in VietNam</h1>
                <nav>
                    <NavLink exact to ="/">Home</NavLink>
                    <NavLink exact to ='/tour-package'>Tour</NavLink>
                    <NavLink exact to ='/blog-travel'>Blog Travel</NavLink>
                    <a href="#contactUs">Contact</a>
                    <a href="#testimonial">Testimonial</a>
                    {!isLoggedIn ? 
                        <NavLink exact to={'/login'}>Login</NavLink> : 
                    ([
                        
                        <NavLink exact to={'/dashboard'}>My profile</NavLink>,
                        <p>{`Welcome back! ${userName}`}</p>
                    ])
                    }
                </nav>
            </div>
            <div className="container-top-bar">
                <div>
                    <div className="sticky">
                        <div onClick={handleClick} id="toggle-icon" className={className}>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div className="button">
                <NavLink  exact to = '/tour-package'>
                    Book a tour!
                    <img alt="arrow redirect" src={arrow} />
                </NavLink>
                </div>
            </div>
            
        </div>
    )
}
export default Navbar;