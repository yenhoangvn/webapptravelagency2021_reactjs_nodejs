import React from 'react';
import {FaPaperPlane} from 'react-icons/fa';
import insta1 from '../containers/HomePage/image/insta1.png';
import insta2 from '../containers/HomePage/image/insta2.png';
import insta3 from '../containers/HomePage/image/insta9.png';
import insta4 from '../containers/HomePage/image/insta3.png';
import insta5 from '../containers/HomePage/image/insta4.png';
import insta6 from '../containers/HomePage/image/insta5.png';
import insta7 from '../containers/HomePage/image/insta6.png';
import insta8 from '../containers/HomePage/image/thum1.webp';
import {FaFacebook} from 'react-icons/fa';
import {FaInstagram} from 'react-icons/fa';
import {FaTwitter} from 'react-icons/fa';


function Footer(){
    return(
        <footer>
            <div className="container">
                <div className="footer-area-outer-grid-4col">
                    <div className="col1-outer">
                        <div className="single-widget">
                            <h6>About us</h6>
                            <p>
                            Address: 6th Floor Sannam Building, 78 Duy Tan str, Cau Giay Dis, Hanoi, Vietnam
                            </p>
                            <p>
                            sales@N'bieinvietnam.com
                            </p>
                            <p>
                            Tel :+84 24 628 31800 / 6658 2064
                            </p>

                        </div>
                    </div>

                    <div className="col1-outer">
                        <div className="single-widget">
                            <h6>Navigation Menu</h6>
                            <p>
                                The world has become so fast paced that people don’t want to stand by reading a page of information, they would
                                much rather look.
                            </p>

                        </div>
                    </div>
                
                    <div className="col1-outer">
                        <div className="single-widget">
                            <h6>Newsletter</h6>
                            <p>
                                Subscribe Newsletter to get the best deal and update information.
                                Your information is safe with us. We don't sell info!
                            </p>
                            <form>
                                <input placeholder="Enter Email" />
                                <button>
                                    <FaPaperPlane />
                                </button>
                                <div></div>
                            </form>

                        </div>
                    </div>

                    <div className="col1-outer">
                        <div className="single-widget">
                            <h6 id="insta-title">Instafeed</h6>
                            <div className="instafeed">
                                <img alt="instagram feeds" src={insta1} />
                                <img alt="instagram feeds" src={insta2} />
                                <img alt="instagram feeds" src={insta3} />
                                <img alt="instagram feeds" src={insta4} />
                                <img alt="instagram feeds" src={insta5} />
                                <img alt="instagram feeds" src={insta6} />
                                <img alt="instagram feeds" src={insta7} />
                                <img alt="instagram feeds" src={insta8} />
                            </div>

                        </div>
                    </div>

                </div>

            </div>
            <div className="footer-bottom">
                <div className="container"></div>
                    <div className="bottom-outer">
                        <div className="container">
                            <div className="flex-bottom">
                                <div>
                                    <p>Copyright ©
                                    2021 All rights reserved | Stay tunned | From VietNam with S2
                                    </p>
                                </div>

                                <div>
                                    <FaFacebook/>
                                    <FaInstagram />
                                    <FaTwitter />
                                   
                                </div>


                            </div>
                        </div>
                        
                    </div>
                
                
            </div>

        </footer>
    )
}
export default Footer;