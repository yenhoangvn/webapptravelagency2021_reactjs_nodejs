import React, { useState, useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
// import {Helmet} from "react-helmet";
import Home from './containers/HomePage/Home';
import Footer from './components/Footer';
import Login from './containers/Login';
import axios from './utility/config.js';
import UserProfile from './containers/UserProfile/UserProfile.js';
import TourPackage from './containers/TourPackages/Product';
import Cart from './containers/ShoppingCart/Cart';
import { useLocalStorage } from 'react-use'
import StripePaymentSuccess from './containers/Payment/payment_success';
import StripePaymentCancel from './containers/Payment/payment_error';
import CheckoutPayment from './containers/Payment/CheckoutPayment';
import Blog from './containers/Blog/Blog'
import SinglePost from './containers/Blog/SinglePost/SinglePost'

function App() {
  const [ isLoggedIn, setIsLoggedIn ] = useState(false);
	
  const [userName, setUserName, removeUserName] = useLocalStorage('user','stranger');
  const [userId, setUserId, removeUserId] = useLocalStorage('userId','');
  const [token, setToken, removeToken] = useLocalStorage('token','');
  const [email, setUserEmail, removeUserEmail] = useLocalStorage('email','');
   
  
	const verify_token = async () => {
		if (token === null) return setIsLoggedIn(false);
		try {
			axios.defaults.headers.common['Authorization'] = token;
			const response = await axios.post('/users/verify_token');
			return response.data.ok ? setIsLoggedIn(true) : setIsLoggedIn(false);
		} catch (error) {
			console.log(error);
		}
	};

	useEffect(() => {
		verify_token();
	}, []);
  
	const login = (token, userName, userId,email) => {
    setUserName(userName);
    setUserId(userId);
    setToken(token);
    setIsLoggedIn(true);
    setUserEmail(email);
	};
	const logout = () => {
    removeUserName(userName);
    removeUserId(userId);
    removeToken(token);
    removeUserEmail(email);
    setIsLoggedIn(false);
	};

  
  return (
    <div className="App">
      {/* <Helmet>
        <title>N'bi in Vietnam - Travel Agency</title>
        <meta charSet="utf-8" />
        <meta name="description" content="travel agency, viet nam, asia" />
        <meta name='keywords' content='travel, Vietnam, Asia, tour, luxury, landviews, amazing'/>
        <meta name='viewport' content='width=device-width, initial-scale=1'/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
      </Helmet> */}
      <Router>
        
        <Route 
            exact 
            path="/cart" 
            render={(props)=>(<Cart {...props}  userId={userId} userName={userName} isLoggedIn={isLoggedIn}/>)} 
          />
  
        <Route 
          exact 
          path="/"
          render={(props)=>(<Home {...props} userId={userId} userName={userName} isLoggedIn={isLoggedIn}/>)}        
        />
        
        <Route
          exact
          path="/login"
          render={(props) => (isLoggedIn ? <Redirect to={'/dashboard'} /> : <Login login={login} {...props} />)}
        />
      
        <Route
          path="/dashboard"
          render={(props) => (!isLoggedIn ? <Redirect to={'/'} /> : <UserProfile userId={userId} userName={userName} isLoggedIn={isLoggedIn} logout={logout} {...props} />)}
        />

        <Route 
          exact 
          path="/tour-package"
          render={(props)=>(<TourPackage {...props} userId={userId} userName={userName} isLoggedIn={isLoggedIn}/>)}        
        />
      
        <Route exact path="/checkout" 
        render={props => <CheckoutPayment {...props} email={email} userName={userName} userId={userId} isLoggedIn={isLoggedIn}/>}
        />
        <Route
        exact
        path="/stripepaymentsuccess"
        render={props => <StripePaymentSuccess {...props} isLoggedIn={isLoggedIn} />}
        />
        <Route
          exact
          path="/payment/error"
          render={props => <StripePaymentCancel {...props} isLoggedIn={isLoggedIn} />}
        />
        <Route
        exact
        path="/blog-travel/"
        render={(props)=>(<Blog {...props} userId={userId} userName={userName} isLoggedIn={isLoggedIn}/>)}
        />
        <Route
        exact
        path="/blog-travel/:postId"
        render={(props)=>(<SinglePost {...props} userId={userId} userName={userName} isLoggedIn={isLoggedIn}/>)}
        />
        
      </Router>
      <Footer />

    </div>
  );
}

export default App;
