import axios from '../utility/config.js';
import React,{useState} from 'react';
function Register(){
    const [form, setFormValues] = useState({});
    const [ message, setMessage ] = useState('');

    const handleChange = ({target})=>{
        const {name, value} = target;
        setFormValues((prev)=>({...prev,[name]:value}));
    }

    const handleSubmit = async(e)=>{
        e.preventDefault();
        try{
            const res = await axios.post('users/register',{
                userName: form.userName,
                email: form.email,
                password: form.password,
                password2: form.password2
            });
            console.log(res);
            setMessage(res.data.message);
            if (res.data.ok) {
				setTimeout(() => {
					window.location = '/login'; // redirect to login page if register success
				}, 2000);
			}
            
        }catch(e){
            console.log(e);
        }
    }

    return(
        <div className="logInsection">
            <h4>Register form</h4>
            <form onSubmit={handleSubmit} onChange={handleChange}>
                <div> 
                    <label>Name</label>
                    <input name="userName" type="text" />
                    <label>Email</label>
                    <input name="email" type="email" />
                    <label>Password</label>
                    <input name="password" type="password"/>
                    <label>Confirm Password</label>
                    <input name="password2" type="password"/>
                </div>
                <button>Register</button>
                <div className="message">
                    <p>{message}</p>
                </div>
            </form>
        </div>
    )
}
export default Register;