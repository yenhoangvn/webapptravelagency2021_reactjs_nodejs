import React, {useEffect,useState} from 'react';
import HeaderParallax from '../TourPackages/components-tourpackages/HeaderScrollParallax';
import NavbarShop from '../TourPackages/components-tourpackages/NavbarShop'
import MainBlog from './MainBlog';
import axios from '../../utility/config';

function Blog({isLoggedIn,userName}){
    const [fullData, setFull] = useState();
    const [done, setDone] =useState(false)
    useEffect(()=>{
        getFull();
    },[done]);
    
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    const getFull = async () => {
    let url1 = '/post/';
    let url2 = '/postCategories/';
    try {
        const res1 = await axios.get(url1);
        const res2 = await axios.get(url2);

        let fullData = {
            post: res1.data,
            postCategoryDB: res2.data
        };
      
        console.log("fullData after call:",fullData);
        setFull(fullData);  
        setDone(true);
    } catch (error) {
        console.log(error);     
    }}
    
    if(!fullData){
        return <p>loading...</p>
    }else{
    return(
        <div>
            <NavbarShop userName={userName} isLoggedIn={isLoggedIn} />
            <HeaderParallax title={'Blog Travel'} />
            <MainBlog fullData={fullData} />

        </div>

    )
}}
export default Blog;