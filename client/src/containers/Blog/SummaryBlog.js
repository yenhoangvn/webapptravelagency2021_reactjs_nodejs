import React,{useEffect, useState} from 'react'
import { FaUser, FaCalendarAlt } from 'react-icons/fa';
import { themeColorState } from '../../utility/State';
import {  useRecoilValue } from 'recoil';
import { NavLink } from 'react-router-dom';

import content1 from './images/m-blog-2.jpeg';
import content2 from './images/m-blog-1.jpeg';
import content3 from './images/m-blog-4.jpeg';
// import thumbnail from './images/Beach , an art print by Sam Yang.jpeg'

function SummaryBlog({fullData,categoryId}){

    const themeColor = useRecoilValue(themeColorState);
    const [chosedCate, setChosedCate] = useState(fullData.post)
    const images=[content1,content2,content3,content1,content3]
    const [postId,setPostId] = useState('')

    useEffect(()=>{
        if(categoryId){
            const chosed = fullData.post.filter(ele=>ele.categoryId === categoryId);
            setChosedCate(chosed);
        }

    },[categoryId])

    let url = `/blog-travel/${postId}`

    const handleClick = (ele,i)=>{
        setPostId(ele._id)
    }
    

    if(!fullData){
        return(
            <p>Loading...</p>
        )
    }
    return(
        <div style={{padding:'2%'}}>
            {
                chosedCate.map((ele,i)=>{
                    return(
                        <div style={{borderBottom:'2px solid #ddd',marginBottom:'4em', paddingBottom:'4em'}}>
                
                            <div className="right-blog-container">
                                <img style={{width:'100%'}} src={images[i]} />

                                <div>
                                    <div style={{display:'flex',flexWrap:'wrap',flexDirection:'row',paddingBottom:'12px'}}>
                                        {fullData.postCategoryDB.map((item,i)=>{
                                            return(
                                            item.category===ele.categoryName ?
                                            <p style={{color:themeColor,fontWeight:'600',padding:'0 1.5%'}}>{`${item.category} `}</p>
                                            :
                                            <p style={{fontSize:'12px',paddingLeft:'5px'}}>{`${item.category} `}</p>

                                        )})}
                                       

                                    </div>
                                    <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:'10%',paddingBottom:'12px',fontWeight:'450',color:'#777777'}}>
                                        <p style={{paddingRight:10}}>{ele.author}</p>
                                        <FaUser style={{fontSize:'20px'}} />

                                    </div>
                                    <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:'10%',paddingBottom:'12px',fontWeight:'450',color:'#777777'}}>
                                        <p style={{paddingRight:10}}>{ele.date}</p>
                                        <FaCalendarAlt style={{fontSize:'20px'}} />

                                    </div>
                                    {/* <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:'10%',paddingBottom:'12px',fontWeight:'450',color:'#777777'}}>
                                        <p style={{paddingRight:10}}>101K Views</p>
                                        <FaEye style={{fontSize:'20px'}} />

                                    </div> */}
                                    {/* <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:'10%',paddingBottom:'12px',fontWeight:'450',color:'#777777'}}>
                                        <p style={{paddingRight:10}}></p>
                                        <FaComment style={{fontSize:'20px'}} />

                                    </div> */}
                                </div>
                            </div>
                            <div style={{width:'75%'}}>
                                <h1 onClick={(ele,i)=>handleClick(ele,i)} className="titleBlog" style={{padding:'2% 0',cursor:'pointer'}}>{ele.title}</h1>
                                <p style={{textAlign:'justify',color:'#777777',marginBottom:'10px',fontFamily:'sans-serif',fontWeight:'400'}}>
                                MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
                                </p>

                            </div>
                            <button onClick={()=>handleClick(ele,i)} className="buttonBlog">
                                <NavLink exact to={url}>Read More</NavLink>
                            </button>
                        </div>

                    )
                })
            }
            
           
            


        </div>
    )
}
export default SummaryBlog;