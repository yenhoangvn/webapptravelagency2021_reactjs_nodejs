import React, { useState } from 'react';
import LeftContent from './LeftContent';
import SummaryBlog from './SummaryBlog';

function MainBlog({fullData}){

    const[categoryId,setCategoryId] = useState()
    
    const receiveData = (categoryId) =>{
        setCategoryId(categoryId)
    }
    return(
        <div className="section-main-shop-tour-package">
            <div className="container">
                <div className="grid-outer">
                    <LeftContent receiveData={receiveData} fullData={fullData} />
                    <SummaryBlog categoryId={categoryId} fullData={fullData} />
                </div>
    
             </div>

        </div>
    )
}
export default MainBlog;