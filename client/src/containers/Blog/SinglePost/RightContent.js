import React,{useState,useEffect} from 'react'
import { FaUser, FaCalendarAlt, FaComment, FaArrowRight, FaArrowLeft } from 'react-icons/fa';
import { themeColorState } from '../../../utility/State';
import {  useRecoilValue } from 'recoil';
import axios from '../../../utility/config'
import banner from '../images/feature-img1.jpeg';
import content from '../images/post-img1.jpeg';
import thumbnail from '../images/Beach , an art print by Sam Yang.jpeg'
import { NavLink } from 'react-router-dom';

function RightContent({fullData,postId,receivedPostId,isLoggedIn,userName}){

    const indexPost = fullData.post.findIndex((ele)=>ele._id === postId)
    const nextIndex = indexPost===fullData.post.length-1 ? 0 : indexPost+1;
    const preveIndex = indexPost === 0 ? fullData.post.length-1 : indexPost-1
    const [text,setText] = useState('')
    const [comment,setComment] = useState(false)
    const [commentDB,setCommentDB] = useState()
    const [dataDone, setDataDone] = useState(false)
    
    let url = `/blog-travel/${postId}`
    
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [postId])

    useEffect(() => {
       
    }, [comment])

    useEffect(() => {
        getCommentDB() 
    }, [dataDone])

    const themeColor = useRecoilValue(themeColorState);

    const getCommentDB = async()=>{
        let url = `/comment/${postId}`
        try{
            const res = await axios.get(url)
            setCommentDB(res.data)
            setDataDone(true)
        }catch(e){
            console.log(e)
        }
    }

    const nextPost = () =>{
        if(indexPost===fullData.post.length-1){
            receivedPostId(fullData.post[0]._id)            
        }else{
            receivedPostId(fullData.post[indexPost+1]._id)

        }
       
    }

    const prevPost = ()=>{
        if(indexPost===0){
            receivedPostId(fullData.post[fullData.post.length-1]._id)
        }else{
            receivedPostId(fullData.post[indexPost-1]._id)

        }

    }

    const CommentContent = (e) =>{
        let value = e.target.value;
        console.log('comment =====>',value)
        setText(value)
    }
    
    const submitComment = async() =>{
        if(isLoggedIn){

        let url = "/comment/create"
        let dateNow = Date().toString()
        try{
            
            const res = await axios.post(url,{
                userName:userName,
                content:text,
                date:dateNow,
                postId:postId
            })
            console.log('comment res here========>',res.data)

        }catch(e){
            console.log(e)
        }
    }else{
        setComment(true)
        setTimeout(()=>window.location='/login',3000)
        clearTimeout()  
        
    }

    }
    console.log('commentDB ===>',commentDB)

   if(!fullData || !commentDB){
       return(
           <p>loading...</p>
       )
   }
    return(
        <div style={{padding:'2%'}}>
            <div style={{borderBottom:'2px solid #ddd',marginBottom:'4em', paddingBottom:'4em'}}>
                <img alt="banner" style={{width:'100%'}} src={banner} />
                <h1 className="titleBlog" style={{padding:'2% 0',cursor:'pointer'}}>{fullData.post[indexPost].title}</h1>
                <div style={{display:'grid',gridTemplateColumns:'20% 80%'}}>

                    <div>
                        <div style={{display:'flex',flexWrap:'wrap',flexDirection:'row',paddingBottom:'12px',marginRight:'10px'}}>
                            {fullData.postCategoryDB.map((item,i)=>{
                                return(
                                item.category===fullData.post[indexPost].categoryName ?
                                <p key={i} style={{color:themeColor,fontWeight:'600',padding:'0 1.5%'}}>{`${item.category} `}</p>
                                :
                                <p key={i} style={{fontSize:'12px',paddingLeft:'5px'}}>{`${item.category} `}</p>

                            )})}

                        </div>
                        <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:'10%',paddingBottom:'12px',fontWeight:'450',color:'#777777'}}>
                            <p style={{paddingRight:10}}>{fullData.post[indexPost].author}</p>
                            <FaUser style={{fontSize:'15px'}} />

                        </div>
                        <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:'10%',paddingBottom:'12px',fontWeight:'450',color:'#777777'}}>
                            <p style={{paddingRight:10,maxWidth:'50%'}}>{fullData.post[indexPost].date}</p>
                            <FaCalendarAlt style={{fontSize:'15px'}} />

                        </div>
                        <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between',marginRight:'10%',paddingBottom:'12px',fontWeight:'450',color:'#777777'}}>
                            <p style={{paddingRight:10}}>{commentDB.length}</p>
                            <FaComment style={{fontSize:'15px'}} />

                        </div>
                       
                    </div>
                    <div>
                        <p className="paragraphBlog">
                        MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction.
                        </p>

                        <p className="paragraphBlog">
                        Boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price.
                        </p>

                        <p className="paragraphBlog">
                        However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to actually sit through a self-imposed.
                        </p>

                        <p className="paragraphBlog">
                        Boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to actually sit through a self-imposed
                        </p>

                    </div>
                

                </div>

                <div>
                    <p style={{fontStyle:'italic',color:'#777',fontWeight:'400',lineHeight:'24px',margin:'20px 0 30px 0'}}>
                    MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training.
                    </p>
                    <div style={{display:'grid',gridTemplateColumns:'50% 50%', gridGap:'3em',margin:'5% 5% 5% 1%'}}>
                        <img alt="content" style={{width:'100%',boxShadow:'0px 2px 10px 20px rgba(238, 238, 238, 0.644)'}} src={content} />
                        <img alt="content" style={{width:'100%',boxShadow:'0px 2px 10px 20px rgba(238, 238, 238, 0.644)'}} src={content} />

                    </div>
                    <p style={{textAlign:'justify',color:'#777777',marginBottom:'10px',marginBottom:'10px',fontFamily:'sans-serif',fontWeight:'400'}}>
                    MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training.
                    </p>
                    <p style={{textAlign:'justify',color:'#777777',marginBottom:'10px',marginBottom:'10px',fontFamily:'sans-serif',fontWeight:'400'}}>
                    MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower.

                    MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower.
                    </p>
                </div>

            </div>
            <div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                <div style={{display:'flex',flexDirection:'row',alignItems:'center'}}>
                    <div onClick={()=>prevPost()} className="slideControllerBlog">
                        <NavLink exact to={url}>
                        <img alt="thumbnail" style={{height:'60px',width:'60px'}} src={thumbnail} />
                        <div className="overLayBlog">
                            <FaArrowLeft />
                        </div>
                        </NavLink>

                    </div>
                    
                    <div style={{marginLeft:'1em'}}>
                        <p>Prev Post</p>
                        <p className="textBoldBlog">{fullData.post[preveIndex].title}</p>

                    </div>

                </div>

                <div style={{display:'flex',flexDirection:'row',alignItems:'center'}}>
                        
                    <div style={{marginRight:'1em',textAlign:'right'}}>
                        <p>Next Post</p>
                        <p className="textBoldBlog">{fullData.post[nextIndex].title}</p>

                    </div>
                    <div onClick={()=>nextPost()} className="slideControllerBlog">
                        <NavLink exact to={url}>
                        <img alt="thumbnail" style={{height:'60px',width:'60px'}} src={thumbnail} />
                        <div className="overLayBlog">
                            <FaArrowRight />
                        </div>

                        </NavLink>


                    </div>
                        

                </div>
            </div>
            {commentDB && commentDB.map((ele,i)=>{
                return(
                    <div key={i} style={{display:'flex',flexDirection:'row',alignItems:'flex-start', margin:'5% 0',backgroundColor:'#fafaff', border: '1px solid #eee', padding:'50px 30px'}}>
                        <div>
                            <FaUser style={{fontSize:'50px',justifySelf:'center',alignSelf:'center'}} />
                        </div>
                        <div style={{marginLeft:'2em'}}>
                            <h5 className="textBoldBlog">{ele.userName}</h5>
                            <p className="textNormalComment">{ele.date}</p>
                            <p style={{color:'#777777'}} className="textNormalComment">{ele.content}</p>
                        </div>


                    </div>

                )
            })}
            
            <div style={{border:'2px solid #ddd',padding:'5% 2%',margin:'10% 0 3% 0'}}>
                <h2>Comments</h2>
                <textarea onChange={(e)=>CommentContent(e)} value={text} style={{height:'9em',width: '100%', padding:'8px 0',fontSize:'14px',color:'#777777'}} placeholder="write something...."></textarea>  
            </div>
            <button onClick={()=>submitComment()} className="buttonBlog" style={{border:'1px solid #ddd',padding:'2% 5%',marginBottom:'10%',fontWeight:'700',fontSize:'13px',color:'#222222'}}>Submit</button>
            { comment && <p style={{marginBottom:'10%',color:'tomato',marginTop:'-8%'}}>Please log in to write a comment!</p>}

            


        </div>
    )
}
export default RightContent;