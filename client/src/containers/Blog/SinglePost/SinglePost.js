import React, {useEffect,useState} from 'react';
import HeaderParallax from '../../TourPackages/components-tourpackages/HeaderScrollParallax';
import NavbarShop from '../../TourPackages/components-tourpackages/NavbarShop'
import LeftContent from '../LeftContent';
import RightContent from './RightContent';
import axios from '../../../utility/config';


function SinglePost(props){
    const {isLoggedIn, userName, userId} = props;
    let postId = props.match.params.postId

    const [fullData, setFull] = useState();
    const [done, setDone] =useState(false)
    const[postInfoID, setPostInfoId] = useState(postId)
    
    console.log('postId====>',postId)
    

    useEffect(()=>{
        getFull();
    },[done,postInfoID]);
    
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    const getFull = async () => {
    let url1 = '/post/';
    let url2 = '/postCategories/';
    try {
        const res1 = await axios.get(url1);
        const res2 = await axios.get(url2);

        let fullData = {
            post: res1.data,
            postCategoryDB: res2.data
        };
      
        console.log("fullData after call======>",res1.data);
        setFull(fullData);  
        setDone(true);
    } catch (error) {
        console.log(error);     
    }}
    

    const receivedPostId = (postId) =>{
        setPostInfoId(postId)
    }
    const receiveData = (data)=>{
        window.location = "/blog-travel"

    }
    
    if(!fullData){
        return <p>loading...</p>
    }else{

    return(
        <div>
            <NavbarShop isLoggedIn={isLoggedIn} />
            <HeaderParallax title={'Blog Travel'} />
            <div className="section-main-shop-tour-package">
            <div className="container">
                <div className="grid-outer">
                    <LeftContent receiveData={receiveData} fullData={fullData}/>
                    <RightContent userName={userName} userId={userId} isLoggedIn={isLoggedIn} fullData={fullData} postId={postInfoID} receivedPostId={receivedPostId} />
                </div>
    
             </div>

        </div>
            
            

        </div>

    )
}}
export default SinglePost;