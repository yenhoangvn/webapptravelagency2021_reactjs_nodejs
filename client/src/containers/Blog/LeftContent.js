import React from 'react';
import { themeColorState } from '../../utility/State';
import {  useRecoilValue } from 'recoil';
import {NavLink} from 'react-router-dom';

function LeftContent({fullData,receiveData}){
    const themeColor = useRecoilValue(themeColorState);
    const styleBackGround = {backgroundColor:themeColor};
    const styleText = {color:themeColor};
    

    
    const displayPost = (ele,i) =>{
        receiveData(ele._id)
    }

    return(
        <div className="left-side-container">
            <div className="browser-categories">
                <div style={styleBackGround} className="title-widget">
                    <h5>Post Category</h5>
                </div>
                <div className="browser-cate-content">
                    <div className="subcategory-container">
                        {
                            fullData.postCategoryDB.map((ele,i)=>{
                                return(
                                    <a style={{cursor:'pointer'}} onClick={()=>displayPost(ele,i)} key={`${ele.title}${i}`} className="container-check-subcategory">
                                        <span style={styleText}>{ele.category}</span>
                                    </a>

                                )
                            })
                        }
                    </div>
                    
                </div>
            </div>
            <div className="browser-categories">
                <div style={styleBackGround} className="title-widget">
                    <h5>Popular Post</h5>
                </div>
                <div className="browser-cate-content">
                <div className="subcategory-container">
                    {
                    fullData.post.map((ele,i)=>{
                        return(
                            <NavLink exact to ={`/blog-travel/${ele._id}`} key={`${ele.title}${i}`} className="container-check-subcategory">
                                <span style={styleText}>{ele.title}</span>
                            </NavLink>        
                        )
                    })
                    }
                    </div>
                </div>


            </div>



        </div>
    )
}
export default LeftContent;