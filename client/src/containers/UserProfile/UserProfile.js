import React, { useEffect, useState } from 'react';
import {NavLink, Link} from 'react-router-dom';
import {FaAngleRight} from 'react-icons/fa';
import NavBar from './NavBarUserProfile';
import test1 from '../ShoppingCart/images/na_vrhova__plocha_1.png';
import { useRecoilValue, useSetRecoilState } from 'recoil';
import { wishListState } from '../../utility/State';
import {orderState} from '../../utility/State';
import {FaTrashAlt} from 'react-icons/fa'

function UserProfile(props){

	const {userName, isLoggedIn, userId} = props;
	const nonvisible = {display:'none'};
	const visible = {display:'block'};
	const [alert,setAlert] = useState([true,false,false,false,false,false,false,false]);
	const [styleHidden,setStyleHidden] = useState([visible,nonvisible,nonvisible,nonvisible,nonvisible,nonvisible,nonvisible,nonvisible]);
	const style ={marginLeft:'30px',color:'rgb(34, 34, 34)'};
	const wishList = useRecoilValue(wishListState);
    const orders = useRecoilValue(orderState);
    const setOrders = useSetRecoilState(orderState);

    const orderFilterUserName = orders.filter(ele=>ele.userId === userId);

	useEffect(()=>{
		 console.log('hiddeen style=====>',styleHidden);
	},[alert,styleHidden])

  
	const handleClick = ({target}) =>{
		const i = target.className;

        let tempAlert = alert;
		let index1 = tempAlert.findIndex(ele=>ele===true);
		tempAlert.splice(index1,1,false)
		tempAlert.splice(i,1,true);
		setAlert(tempAlert);

		let tempStyleHidden = styleHidden;
		let index2 = tempStyleHidden.findIndex(ele=>ele.display==='block');
		tempStyleHidden.splice(index2,1,nonvisible);
		tempStyleHidden.splice(i,1,visible);
		setStyleHidden(tempStyleHidden);
	}
	const stylePadding = {paddingTop:'130px'}
	const deleteOrder = (ele,i) =>{
		let tempOrder = orders
		let newTempOrder = tempOrder.filter((ele,index)=> index!==i)
		setOrders(newTempOrder)
	}
    return (
        <section>
			<NavBar isLoggedIn={isLoggedIn} userName={userName} />
			<div style={stylePadding}>
				<div className="container">
				    <div className="bread-crumb-flex-outer">
                        <NavLink exact to="/">Home
                            <FaAngleRight />
                        </NavLink>
                        <NavLink exact to="/tour-package">Shop
                            <FaAngleRight />
                        </NavLink>
                        <span>My Profile</span>
                    </div>
					<div className="main-content-user-profile">
						<div className="col1-user-profile">
							<div><Link className="0" style={alert[0] ? style : null} onClick={handleClick}>My Orders</Link></div>
							<div><Link className="1" style={alert[1] ? style : null} onClick={handleClick}>Wish List</Link></div>
							<div><Link className="2" style={alert[2] ? style : null} onClick={handleClick}>Factura</Link></div>
							<div><Link className="3" style={alert[3] ? style : null} onClick={handleClick}>Payment Method</Link></div>
							<div><Link className="4" style={alert[4] ? style : null} onClick={handleClick}>Shipping Information</Link></div>
							<div><Link className="5" style={alert[5] ? style : null} onClick={handleClick}>Personal Information</Link></div>
							<div><Link className="6" style={alert[6] ? style : null} onClick={handleClick}>Account Information</Link></div>
							<div><Link className="7" style={alert[7] ? style : null} onClick={handleClick}>Customer Service</Link></div>
							<div className="log-out">
								<button
									onClick={() => {
										props.history.push('/');
										props.logout();
									}}>
								Sign Out
								</button> 
								
							</div>
						</div>
						<div className="col1-user-profile">
							{/* part my orders */}	
							<div style={styleHidden[0]}  >
								<div className="title-orders-profile">
									<h1>Orders List</h1>
									<span>Displaying your order's information, status and also you can cancel your order from here</span>
								</div>
								<div className="wish-list-display">
								{ orders.length===0 ? 
								<div style={styleHidden[0]} className="1">
									<div className="icon-empty-orders">
										<img alt="empty-shopping-cart" src={test1} />
										<p style={{alignSelf:'center',paddingTop:'2%'}}>Oops! No Orders found</p>
									</div>
							    </div>

								:
								orderFilterUserName.length === 0 ?
								<div style={styleHidden[0]} className="1">
									<div className="icon-empty-orders">
										<img alt="empty-shopping-cart" src={test1} />
										<p style={{alignSelf:'center',paddingTop:'2%'}}>Oops! No Orders found</p>
									</div>
							    </div>
								:
								  orderFilterUserName.map((ele,i)=>{
									return(
											<div key={i}>
												<h4 style={{fontWeight:'bold',paddingBottom:'2%'}}>{`Order: ${i}`}</h4>
												<div>
													<img alt="tour-package" src={ele.item[0].images} />
	
												</div>
												<div style={{display:'flex',flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
													<div>
														<p style={{padding:'1% 0',fontFamily:'monospace'}}>{ele.date}</p>
														<p style={{paddingBottom:'1%',fontFamily:'monospace'}}>{`SubTotal: ${ele.expense}`}</p>
													</div>
													<div style={{marginRight:'12%'}} onClick={()=>deleteOrder(ele,i)}>
													<FaTrashAlt />
													</div>
												</div>
												
											</div>
									)

								})}
								</div>
								

							</div>
							
							
                            {/* part personal infos */}
							<div style={styleHidden[5]} className="title-orders-profile">
								<h1>Personal Information</h1>
								<div className="grid-personal-info-user-profile" >
									<div className="check-box">
										<label className="container-radio-button">Female
											<input type="checkbox" />
											<span className="checkmark"></span>
										</label>
										<label className="container-radio-button">Male
											<input type="checkbox" />
											<span className="checkmark"></span>
										</label>
									</div>
									
									<div>
										<form>
											<div className="personal-infor-user-profile"> 
												<div>
													<label>First Name</label>
													<input type="text" />
												</div>
												<div>
													<label>Last Name</label>
													<input type="text"/>
												</div>
												<div>
													<label>Country</label>
													<input type="text"/>
												</div>
												<div>
													<label>Address</label>
													<input type="text"/>
												</div>
												<div>
													<label>Telefono</label>
													<input type="text"/>
												</div>
											</div>
											<button>Save</button>
											
										</form>
									</div>
								</div>
							</div>

							{/* part account infos */}
							<div style={styleHidden[6]} className="title-orders-profile">
								<h1>Account Information</h1>
								<div className="grid-personal-info-user-profile" >									
									<div>
										<form>
											<div className="personal-infor-user-profile"> 
												<div>
													<label>userName</label>
													<input type="text" />
												</div>
											
												<div>
													<label>PassWord</label>
													<input type="password"/>
												</div>
												<div>
													<label>Email</label>
													<input type="email"/>
												</div>
												<div>
													<label>New password</label>
													<input type="password"/>
												</div>
											</div>
											<button>Save Change</button>
											
										</form>
									</div>
								</div>
							</div>

							{/* part shipping information */}
							<div style={styleHidden[4]} className="title-orders-profile">
								<h1>Shipping Information</h1>
								<div className="grid-personal-info-user-profile">
								<button id="add-new-shipping">Add new shipping address</button>
								</div>
							</div>

							{/* part wishlish */}
							<div style={styleHidden[1]}  >
								<div className="title-orders-profile">
									<h1>Wish List</h1>
									<span>Save all your favorite items here</span>
								</div>
								<div className="wish-list-display">
								{ wishList.length===0 ? <p>Oops! you haven't add any product</p> :
								  wishList.map((ele,i)=>{
									return(
											<div key={i}>
												<img alt="tour-thumbnail" src={ele.images} />
												<p>{ele.product}</p>
											</div>

										
									)

								})}
								</div>
								

							</div>



						</div>
						
						
					</div>
					

				</div>

			</div>
            
			

        </section>
    )
}
export default UserProfile;
