import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import {FiUser} from 'react-icons/fi';
import { FiHeart } from "react-icons/fi";
import {FiShoppingCart} from 'react-icons/fi';
import { useRecoilValue } from 'recoil';
import { cartState } from '../../utility/State';
import { wishListState } from '../../utility/State';
import { themeColorState } from '../../utility/State';

function Navbar({isLoggedIn,userName}){

    const user = userName.split(" ");
    const [style, setStyle] = useState({display:'none'});
    const [className, setClassName] = useState();

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])
      
    const handleClick = () =>{
        if(style.display==='none'){
            setStyle({display:'grid'})
            setClassName('visible');
        }else{
            setStyle({display:'none'})
            setClassName('');
        }
    }
    const cart = useRecoilValue(cartState);
    const wishList = useRecoilValue(wishListState);
    const themeColor = useRecoilValue(themeColorState);
    const styleBackGround = {backgroundColor:themeColor};
  
    const [colorChange, setColorchange] = useState(false);
    const changeNavbarColor = () =>{
        window.scrollY > 0 ? setColorchange(true) : setColorchange(false)
    }
    const style1 = {backgroundColor:'transparent'}
    const style2 = 
    {
        backgroundColor:'#fff',
        boxShadow:'0 0 3px 0 rgb(0 0 0 / 20%)'
    }    
    window.addEventListener('scroll', changeNavbarColor);
    return(
        <div style={colorChange ? style2 : style1} id="sticky-top-bar" className="top-bar-wrapper">
            <div style={style} className="side-menu-hidden">
                <h1>N'bie in VietNam</h1>
                <nav>
                    <NavLink exact to ="/">Home</NavLink>
                    <NavLink exact to ='/tour-package'>Tour</NavLink>
                    <NavLink exact to ='/blog-travel'>Blog Travel</NavLink>
                    <NavLink exact to ='/' href="#contactUs">Contact</NavLink>
                    <NavLink exact to ='/' href="#testimonial">Testimonial</NavLink>
                    {!isLoggedIn ? 
                    <NavLink exact to={'/login'}>Login</NavLink>: 
                    <NavLink exact to={'/dashboard'}>My profile</NavLink>
                    }
                </nav>
            </div>
           
            <div className="container-top-bar">
                <div>
                    <div>
                        <div onClick={handleClick} id="toggle-icon" className={className}>
                            <span></span>
                        </div>
                    </div>
                </div>
                <div id="user-top-bar">
                    <div className="icon-user">
                        {isLoggedIn ? <NavLink id="help-user-profile" exact to ="/dashboard">Hi! {user[0]}</NavLink> :
                        <NavLink exact to ="/login"><FiUser /></NavLink>
                        }
                    </div>
                    <div className="icon-user">
                        {isLoggedIn ? <NavLink exact to ="/dashboard"><FiHeart /></NavLink> : <NavLink exact to ="/login"><FiHeart /></NavLink> }
                        <p style={styleBackGround} id="wish-list-notify-icon">{wishList.length}</p>
                    </div>
                    <div className="icon-user">
                        <NavLink exact to ="/cart">
                            <FiShoppingCart/>
                            <p style={styleBackGround} id="wish-list-notify-icon">{cart.length}</p>
                        </NavLink>
                        
                    </div>
                </div>
            </div>
            
        </div>
    )
}
export default Navbar;





