import React, {useState,useEffect} from 'react';
import { NavLink, Link } from 'react-router-dom';
import axios from '../utility/config.js';
import NavBar from './TourPackages/components-tourpackages/NavbarShop';
import {FaAngleRight} from 'react-icons/fa';
import Register from './Register';

function Login(props){
    const [ formState, setFormState ] = useState({
		email: '',
		password: ''
	});
	const [ message, setMessage ] = useState('');
    
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    //handleChange to take and save user datas from all inputs in the form in the state
    const handleChange = ({target}) => {
        const {name,value} = target; 
        setFormState((prev)=>({...prev,[name]:value}))
	};
    
    //handleSubmit will post user datas to DB and take back user token
    //handSubmit = postData
    const handleSubmit = async (e) => {
		e.preventDefault();
		try {
            console.log(formState);
			const response = await axios.post('/users/login', {
				email: formState.email,
				password: formState.password
			});

			setMessage(response.data.message);
            
			if (response.data.ok) {            
				setTimeout(() => {
					props.login(response.data.token, response.data.userName, response.data.userId); // pass back token to app.js through login func
					props.history.push('/dashboard'); 
				}, 2000);
			}
		} catch (error) {
			console.log(error);
		}
	};
    
    const stylePadding = {paddingTop:'120px'}
    return(
        <section className="login-register-page">
            <NavBar />
            <div style={stylePadding} className="container">
                <div className="bread-crumb-flex-outer">
                    <NavLink exact to="/">Home
                        <FaAngleRight />
                    </NavLink>
                    <NavLink exact to="/tour-package">Shop
                        <FaAngleRight />
                    </NavLink>
                    <span>My Profile</span>
                </div>

                <div className="loginContainer">
                    <div className="logInsection">
                        <h4>Sign In</h4>
                        
                        <form onSubmit={handleSubmit} onChange={handleChange}>
                            <label>Email</label>
                            <input name="email" type="email" />
                            <label>Password</label>
                            <input name="password" type="password" />
                            <button>Sign In</button>
                            <Link href="#">Have you forgotten your password?</Link>
                            <div className="message">
                                <p>{message}</p>
                            </div>
                        </form>
                    </div>
                    <div className="middle-logIn"></div>
                    <div>
                        <Register />
                    </div>

                </div>
            </div>
        </section>
    )
}
export default Login;