import React, { useState, useMemo }  from "react";
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { cartState } from '../../utility/State';
import {orderState} from '../../utility/State';
import axios from '../../utility/config';
import { useStripe, useElements, CardElement } from "@stripe/react-stripe-js";
import useResponsiveFontSize from './useResponsiveFontSize';

const useOptions = () => {
  const fontSize = useResponsiveFontSize();
  const options = useMemo(
    () => ({
      style: {
        base: {
          fontSize,
          color: "#424770",
          letterSpacing: "0.025em",
          fontFamily: "Source Code Pro, monospace",
          "::placeholder": {
            color: "#aab7c4"
          }
        },
        invalid: {
          color: "#9e2146"
        }
      }
    }),
    [fontSize]
  );

  return options;
};


function CheckoutForm(props){
    const stripe = useStripe();
    const elements = useElements();
    const {userId,subTotal,email,userName} = props;
    const options = useOptions();
    const [isPaymentLoading, setPaymentLoading] = useState(false);
    const cart = useRecoilValue(cartState);
    const setCart = useSetRecoilState(cartState);
    const orders = useRecoilValue(orderState);
    const setOrders = useSetRecoilState(orderState);
    
    const sendTokenToServer = async(pmId)=>{
        try {
            const response = await axios.post(
                "/payment/create_payment",
                {
                  subTotal: subTotal,
                  pmId: pmId,
                  userId: userId,
                  email:email,
                  name:userName
                }
            );
            if (response.data.ok){
                console.log('response status =====>',response.data.ok)
                let tempOrder = orders;
                  setOrders([...tempOrder,
                    { 
                        userId:userId,
                        date: new Date().toDateString(),
                        payment:
                        {
                          paymentId:pmId,
                          paymentStatus:'done'
                        },
                        expense:subTotal,
                        item:cart
                    }
                  ])
                setCart([]);
                
                window.location = '/stripepaymentsuccess' 
               
            }else{
                window.location = '/stripepaymentcancel' 
            }
        }catch(e){
            console.log(e)
        }
    } 
    const sendData = async () =>{
      try{
          if (!stripe || !elements) {
              return;
            }
          setPaymentLoading(true);

          const cardElement = elements.getElement(CardElement);
          const pm = await stripe.createPaymentMethod({
              type: 'card',
              card: cardElement,
          });
          if(pm?.paymentMethod?.id){
              sendTokenToServer(pm.paymentMethod.id);
              console.log('pmid ====>',pm.paymentMethod.id)
          } else {
              alert('wrong information provide, please check it again!')
          } 
        }catch(e){
            console.log(e);
        }    
    }

   
    const styleForm = {width:'60%'};
    const styleButton = {marginTop:'20px'}
    
    return(
        <>
        <form className="paymentform" style={styleForm} >
            <label>Your email</label>
            <input />
            <label>Card details</label>
                
            <CardElement
                options={options}
                onReady={() => {
                console.log("CardElement [ready]");
                }}
                onChange={event => {
                console.log("CardElement [change]", event);
                }}
                onBlur={() => {
                console.log("CardElement [blur]");
                }}
                onFocus={() => {
                console.log("CardElement [focus]");
                }}
            />
            
            <button className="pay-button" style={styleButton} type="button" onClick={sendData}>
            { isPaymentLoading ? 'Loading...' : `Pay €${subTotal}`}
            </button>
   
         </form>
        
         </>
    )
}
export default CheckoutForm;

