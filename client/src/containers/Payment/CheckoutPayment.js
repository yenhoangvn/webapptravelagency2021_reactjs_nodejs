import React from 'react';
import { cartState } from '../../utility/State';
import { useRecoilValue } from 'recoil';
import Checkout from './checkout';
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import { BrowserRouter } from "react-router-dom";
import NavBar from '../UserProfile/NavBarUserProfile';
import {NavLink} from 'react-router-dom';
import {FaAngleRight} from 'react-icons/fa';
import {shippingInfoState} from '../../utility/State';

const stripePromise = loadStripe("pk_test_51JCLF3AULZ5STiFkbAy6YuzPfLQMMzwouYfPiaGKevFCAhQv4rua4qZ0wCxcKFBwQCp4bBWU2LSEQI7NHFoJ5OKP00TCeLBq22");
function CheckoutPayment ({ isLoggedIn,userName,userId,email }){

    const shippingInfo = useRecoilValue(shippingInfoState);
    // const user = userName.split(" ");
    const cart = useRecoilValue(cartState);
    let cartTotal = [];
    cart.forEach(ele =>{
        let total = Number(ele.quantity * ele.price);
        cartTotal.push(total);
    })
    let subTotal = cartTotal.reduce((accumulator, currentValue)=>accumulator+currentValue,0);
    const style = {paddingTop:'130px'}
    return(
        <div>
            <NavBar isLoggedIn={isLoggedIn} userName={userName} />
            <div style={style}>
                <div className="container">
                    <div className="bread-crumb-flex-outer">
                        <NavLink exact to="/">Home
                            <FaAngleRight />
                        </NavLink>
                        <NavLink exact to="/tour-package">Shop
                            <FaAngleRight />
                        </NavLink>
                        <NavLink exact to="/cart">cart
                            <FaAngleRight />
                        </NavLink>
                        <span>Checkout</span>
                    </div>
            

            <div className="table-wrapper">

                <div className="cart-col2">
                    <div className="total-outer">
                        <h4>Payment details</h4>
                        <div className="subtotal-cart">
                            <span>Subtotal</span>
                            <span>{subTotal}</span>
                        </div>
                        <div className="shipping-infos-cart">
                            <span>Shipping Address</span>
                            <div className="main-shipping-cart">
                                <p>
                                    {`${shippingInfo.street}, ${shippingInfo.city}, ${shippingInfo.country}, ${shippingInfo.zip}`}
                                </p>
                            </div>
                        </div>
                        <div className="shipping-infos-cart">
                            <span>Shipping Method</span>
                            <div className="main-shipping-cart">
                                <p>
                                    STANDARD SHIPPING
                                </p>
                            </div>
                        </div>
                        <div className="shipping-infos-cart">
                            <span>Payment Method</span>
                            <BrowserRouter>
                                <Elements stripe={stripePromise}>
                                    <Checkout email={email} userName={userName} userId={userId} subTotal={subTotal}/>
                                </Elements>
                            </BrowserRouter>
                        </div>

                    </div>
                </div>
            
            
            </div>

           
            </div>
        {/* </div> */}
        </div>

        </div>

    )
}
export default CheckoutPayment;