import React from "react";
import NavBar from '../TourPackages/components-tourpackages/NavbarShop';
import {NavLink} from 'react-router-dom';
import {FaAngleRight} from 'react-icons/fa';
import {FaCheckCircle} from 'react-icons/fa';


const StripePaymentSuccess = ({isLoggedIn}) => {
  const style = {paddingTop:'130px',marginBottom:'100px'}
    return (
      <section>
      <NavBar isLoggedIn={isLoggedIn}/>
      <div style={style}>
          <div className="container">
              <div className="bread-crumb-flex-outer">
                  <NavLink exact to="/">Home
                      <FaAngleRight />
                  </NavLink>
                  <NavLink exact to="/tour-package">Shop
                      <FaAngleRight />
                  </NavLink>
                  <span>success paid</span>
              </div>

              <div className="main-confirm-success">
                  <div id="payment-success-icon" className="iconCheck">
                      <FaCheckCircle/>
                  </div>
                  <h2>Thanks for your order!</h2>
                  <h4>Your payment is successful.</h4>

              </div>
          </div>
      </div>

      </section>
    );
  };
  export default StripePaymentSuccess;