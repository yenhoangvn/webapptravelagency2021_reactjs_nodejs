import React from 'react';
import emptyCart from './images/shoppingcartEmpty.png';
import {NavLink} from 'react-router-dom';
function EmptyCart(){
    
    return(
        <div id="loggedIn-empty-cart" className="outer-empty-shopping-cart">
            <div className="container">
                <div className="empty-cart-display">
                    <img alt="empty cart icon" src={emptyCart} />
                    <h4>Your cart is empty</h4>
                    <p>Shop now to explore our special tours</p>
                    <button><NavLink exact to ="/tour-package">Book A Tour</NavLink></button>
                </div>
            </div>

    </div>
    )
}
export default EmptyCart;