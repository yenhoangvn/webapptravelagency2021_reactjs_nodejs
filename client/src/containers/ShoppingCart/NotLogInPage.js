import React from 'react';
import emptyCart from './images/shoppingcartEmpty.png';
import {NavLink} from 'react-router-dom';
function NotLogInCart(){
    return(
        <div className="outer-empty-shopping-cart">
            <div className="container">
                <div className="empty-cart-display">
                    <img alt="empty cart icon" src={emptyCart} />
                    <h4>Sign In to see your cart</h4>
                    <p>Sign In and Start booking your dream tour now!</p>
                    <button><NavLink exact to ="/login">Register / Sign In</NavLink></button>
                    <button><NavLink exact to ="/tour-package">Book A Tour</NavLink></button>
                </div>
            </div>

        </div>
    )
}
export default NotLogInCart;