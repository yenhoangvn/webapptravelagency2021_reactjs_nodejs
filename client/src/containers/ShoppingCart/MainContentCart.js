import React from 'react';
import {NavLink} from 'react-router-dom';
import {FaAngleRight} from 'react-icons/fa';
import EmptyCart from './EmptyCart';
import CartNonEmpty from './CartNonEmpty';
import { cartState } from '../../utility/State';
import { useRecoilValue } from 'recoil';

function MainCart(){
    const cart = useRecoilValue(cartState);
    const style = {paddingTop:'130px'}
        return(
            <div style={style}>
                <div className="container">
                    <div className="bread-crumb-flex-outer">
                        <NavLink exact to="/">Home
                            <FaAngleRight />
                        </NavLink>
                        <NavLink exact to="/tour-package">Shop
                            <FaAngleRight />
                        </NavLink>
                        <span>ShoppingCart</span>
                    </div>
                 
                    {cart.length === 0 ? <EmptyCart /> : <CartNonEmpty />}
                </div>
    
            </div>
        )

}
export default MainCart;