import React,{useEffect} from 'react';
import NavBar from '../TourPackages/components-tourpackages/NavbarShop';
import MainCart from './MainContentCart';
import NotLogInCart from './NotLogInPage';

function ShoppingCart({userId,userName,isLoggedIn,shoppingDB}){
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])
    return(
        <section>
            <NavBar isLoggedIn={isLoggedIn} />
            {isLoggedIn ?
              <MainCart shoppingDB={shoppingDB} userId={userId} userName={userName} isLoggedIn={isLoggedIn} /> :
              <NotLogInCart />
            } 
        </section>
    )
}
export default ShoppingCart;