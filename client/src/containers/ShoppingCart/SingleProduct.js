import React, { useEffect } from 'react';
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { cartState } from '../../utility/State';

function SingleProduct({productInfo}){

    const cart = useRecoilValue(cartState);
    const setCart = useSetRecoilState(cartState);
    // const cartFilterUserId = wishList.filter(ele=>ele.userId === userId)

    useEffect(()=>{

    },[cart])
   
    let updateCart = (prod,operation) => {
        let tempCart = cart;
        let index = cart.findIndex(ele=>ele._id===prod._id);
        if(operation==='+'){
            let oldQuantity = Number(cart[index].quantity);
            let newQuantity = oldQuantity+1;
            let newTempCart = tempCart.map((element,i) => i === index ? {...element, quantity : newQuantity} : element);
            setCart(newTempCart);
        }else if(operation==='-'){
            let oldQuantity = Number(cart[index].quantity);
            let newQuantity = oldQuantity-1;
            let newTempCart = tempCart.map((element,i) => i === index ? {...element, quantity : newQuantity} : element);
            setCart(newTempCart);
        }else{
            let newQuantity = Number(operation);
            let newTempCart = tempCart.map((element,i) => i === index ? {...element, quantity : newQuantity} : element);
            setCart(newTempCart);
        }
    }
    let deleteTour = (id)=>{
        let tempCart = cart;
        let index = cart.findIndex(ele=>ele._id===id);
        let newTempCart = tempCart.filter((element,i) => i!==index);
        setCart(newTempCart);
    }
    let updateProduct = cart.find(ele=>ele._id===productInfo._id);
    return(
        <div id="grid-product-row2" className="grid-header-row1">
            <div id="thumbnail-cart">
                <div className="thumbnail-shopping-cart-tour">
                    <img alt="tour-package" src={productInfo.images} />
                    <div className="overlay-delete">
                        <span id={productInfo.productId} onClick={()=>deleteTour(productInfo._id)}>x</span>
                    </div>
                </div>
                <p>{productInfo.product}</p>
            </div>

            <span>{productInfo.price}</span>
           
            <div className="outer-quantity-controller">
                    <div onClick={(e)=> updateCart(productInfo,"-")} className="substract">-</div>
                            
                    <input onChange={({target})=>updateCart(productInfo,target.value)} name="quantityTour" className="quantity" type="number" value={productInfo.quantity} />

                    <div onClick={(e)=> updateCart(productInfo, "+")} className="substract">+</div>
            </div>
            <span>{productInfo.price*updateProduct.quantity}</span>
        </div>

    )
}
export default SingleProduct;