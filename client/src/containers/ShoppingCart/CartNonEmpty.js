import React, { useEffect, useState } from 'react';
import SingleProduct from './SingleProduct';
import { cartState } from '../../utility/State';
import {shippingInfoState} from '../../utility/State';
import { useSetRecoilState, useRecoilValue } from 'recoil';
import {NavLink} from 'react-router-dom';


function CartNonEmpty(){
    const cart = useRecoilValue(cartState);
    // const setCart = useSetRecoilState(cartState);
    const shippingInfo = useRecoilValue(shippingInfoState);
    const setShippingInfo = useSetRecoilState(shippingInfoState);
    useEffect(()=>{
    },[cart])
    let cartTotal = [];
    cart.forEach(ele =>{
        let total = Number(ele.quantity * ele.price);
        cartTotal.push(total);
    })
    let subTotal = cartTotal.reduce((accumulator, currentValue)=>accumulator+currentValue,0)
    const [address,setAddress] = useState({
        street:'',
        city:'',
        country:'',
        zip:''
    })
    useEffect(()=>{

    },[address,shippingInfo])

    const getAddress = ({target})=>{
        const {name,value} = target;
        setAddress((prev)=>({...prev,[name]:value}));
        setShippingInfo(address);
    }
  
    return(
        <div className="table-wrapper">
            <div className="container">
                <div className="grid-mainCart-outer-2col">
                    <div className="cart-col1">
                        <div className="outer-cart-col1">
                            <div className="grid-header-row1">
                                <h6>Product</h6>
                                <h6>Price</h6>
                                <h6>Quantity</h6>
                                <h6>Total</h6> 

                            </div>
                            
                            {cart.map((ele,i)=>{
                                return(
                                    <SingleProduct productInfo={ele} key={i} />
                                    )
                            })}
                            
                            <div className="grid-product-cart-row3">
                                <form>
                                    <input placeholder="Coupon code" />
                                    <button>Apply Coupon</button>
                                </form>
                                <button>Update Cart</button>

                            </div>
                            
                        </div>


                    </div>

                    <div className="cart-col2">
                        <div className="total-outer">
                            <h4>Cart Totals</h4>
                            <div className="subtotal-cart">
                                <span>Subtotal</span>
                                <span>{subTotal}</span>
                            </div>
                            <div className="shipping-infos-cart">
                                <span>Shipping</span>
                                <div className="main-shipping-cart">
                                    <p>
                                        Shipping Standard
                                    </p>
                                    <div className="address-query-form-cart">
                                        <span>Shipping address</span>
                                        <form onChange={getAddress}>
                                        <div>
                                            <input name="street" value={address.street} placeholder="street" />
                                        </div>
                                        <div>
                                            <input name="city" value={address.city} placeholder="city" />
                                        </div>
                                        <div>
                                            <input name="country" value={address.country} placeholder="country" />
                                        </div>
                                        <div>
                                            <input name="zip" value={address.zip} placeholder="postcode/zip" />
                                        </div>
                                        </form>
                                        
                                    </div>

                                </div>

                            </div>
                            <div id="subtotal-final" className="subtotal-cart">
                                <span>Total</span>
                                <span>{subTotal}</span>
                            </div>
                            
                            <button id="check-out-button"><NavLink exact to ="/checkout">Check out</NavLink></button>
                        </div>

                    </div>

                </div>

            </div>
         </div>

    )
}
export default CartNonEmpty;
