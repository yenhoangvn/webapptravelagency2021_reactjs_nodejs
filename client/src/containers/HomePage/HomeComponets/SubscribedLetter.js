import React from 'react';
import {NavLink} from 'react-router-dom';
import arrow from '../image/download.webp';
function SubscribeNewsletter(){
    return(
        <section>
            <div className="container-fluid">
                <div className="subscribe-wrapper">
                    <div className="subscribe-title">
                        <div>
                        <h1>Subscribe The Newsletter</h1>
                        <p>
                            To get the best monthly deal
                        </p>

                        </div>

                        <div className="input-group">
                            <input placeholder="Email Address" />
                            <div className="button">
                                    <NavLink exact to = '/'>
                                        Subcribe
                                        <img alt="arrow" src={arrow} />
                                    </NavLink>
                            </div>
    
                        </div>
                        <div></div>
                    </div>

                </div>

            </div>

        </section>
    )
}
export default SubscribeNewsletter;