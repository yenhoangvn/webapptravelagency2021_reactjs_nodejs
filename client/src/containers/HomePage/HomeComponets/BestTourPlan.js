import React,{ useState } from 'react';
import { NavLink } from 'react-router-dom';
import arrow from '../image/download.webp';
import tour1 from '../image/tourplan1.png';
import tour2 from '../image/tourplan2.png';
import tour3 from '../image/tourplan3.png';
import tour4 from '../image/tourplan4.png';
import tour5 from '../image/tourplan5.png';
import tour6 from '../image/tourplan6.png';
import nextButton from '../image/cnext.png.webp';
import previousButton from '../image/cprev.png.webp';
import {FaStar} from 'react-icons/fa';
import {FaThumbtack} from 'react-icons/fa';
import { useRecoilValue } from 'recoil';
import { productDBModifyVersion } from '../../../utility/State';


function BestTourPlan(){
    const [currentSlide, setCurrentSlide] = useState(0);
    const productState = useRecoilValue(productDBModifyVersion);
    const price = [];
    const tourInfo = [];
    productState.forEach(ele=>{
        if(ele.bestSeller){
            price.push(ele.price);
            tourInfo.push(ele.product);
        }   
    })
    console.log('best seller tour plan',tourInfo)
    const images = [[tour1,tour5],[tour3,tour4],[tour2,tour6]];
    const tourInfos=[[tourInfo[0],tourInfo[1]],[tourInfo[2],tourInfo[3]],[tourInfo[4],tourInfo[5]]];
    const tourPrice=[[price[0],price[1]],[price[2],price[3]],[price[4],price[5]]];
    
    //   useEffect(()=>{
    //     const autoSlide = setInterval(()=>{
    //       currentSlide === images.length-1 ?
    //       setCurrentSlide(0):setCurrentSlide(currentSlide+1)
    //     }, 4000);
    //   return () => clearInterval(autoSlide);
    //   },[currentSlide]);
  
    const previous = () =>{
        if(currentSlide>0){
            setCurrentSlide(currentSlide-1);
        }else{
            setCurrentSlide(images.length-1);
        }
    }

    const next = () =>{
        if(currentSlide === images.length-1){
            setCurrentSlide(0);
        }else{
            setCurrentSlide(currentSlide+1);
        }
    }

    return (
        <div className="best-tour-plan">
            <div className="textInfo-container">
                <h1>Best Tour Plan Recently</h1>
                <p>
                “If you don’t know where you are going, 
                you’ll end up someplace else.”
                So, all you need is visit us here, click the 
                button below -> read the plan and take the
                courage to press on to a trip taking you to a whole new destination in
                a whole new country.
                </p>
                <p>
                    Check out all the packages that we have been prepared especially for you S2!
                    Only from 50€ you can enjoy a dream trip to VietNam!
                </p>
                <div id="button-best-tour" className="button">
                    <NavLink exact to ='/tour-package'>
                        Persue All Packages
                        <img alt="arrow redirect" src={arrow} />
                    </NavLink>
                </div>

            </div>
            <div className="img-slideShow">
                <section className="current-slide">
                    <div className="slide-thumbnail">
                        <div className="single-image-wrapper">
                            <img alt="best-guided-tour" src={images[currentSlide][0]}/>
                            <NavLink exact to="/tour-package">
                            <div className="layer">
                                <div className="icon-pin">
                                    <FaThumbtack />
                                </div>
                                
                                <p>Proper Guided Tour</p>
                                <h5>€{tourPrice[currentSlide][0]}</h5>
                                <h4>{`${tourInfos[currentSlide][0]}`}</h4>
                                <p>Proper Guided Tour</p>
                                <div className="review-star">
                                    <FaStar />
                                    <FaStar />
                                    <FaStar />
                                    <FaStar />
                                    <FaStar />
                                </div>
                            </div>
                            </NavLink>
                        </div>
                        
                    </div>
                </section>
                <section className="next-slide">
                    <div className="slide-thumbnail">
                        <div className="single-image-wrapper">
                            <img alt="current vietnam" src={images[currentSlide][1]} /> 
                            <div className="layer">                           
                               <div className="icon-pin">
                                    <FaThumbtack />
                                </div>
                                {/* <NavLink exact to={`/packages/${currentSlide}`}> </NavLink>   */}
                                <p> Proper Guided Tour</p>
                                <h5>€{tourPrice[currentSlide][1]}</h5>
                                <h4>{`${tourInfos[currentSlide][1]} dream holiday and fun package`}</h4>
                                <p>Proper Guided Tour</p>
                                <div className="review-star">
                                    <FaStar />
                                    <FaStar />
                                    <FaStar />
                                    <FaStar />
                                    <FaStar />
                                </div>

                            </div>
                        </div>
                        
                    </div>
                </section>
                
                <div className="slide-controllers">
                    <img alt="previous vietnam" onClick={previous} src={previousButton} />
                    <div></div>
                    <img alt="next vietnam" onClick={next} src={nextButton} />
                </div>

            </div>

        </div>
    )
}
export default BestTourPlan;