import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
// import header1 from '../image/amazingVietNam.jpeg';
// import banner1 from '../image/banner.webp';
import arrow from '../image/download.webp';
// import banner2 from '../image/banner2.jpeg';
import banner from '../image/amazingVietNam1.png';
// import banner3 from '../image/banner3.jpeg';
// import banner5 from '../image/banner5.jpeg';

function HeaderH(){
    const [play,setPlay] = useState(false);
    useEffect(()=>{

    },[play])
    const url = 'https://www.youtube.com/embed/JFhhAOtrO9s?autoplay=1';
    return(
        <div className="headerHContainer">
            <div className="header-Homepage">

                <div className="header-right">
                    <h1>N'bie in VietNam - Vietnamese Travel Agency</h1>
                    <p> 
                        The real voyage of discovery consists not in seeking new landscapes, but in having new eyes.
                        we are here to be your fellow in your trip to VietNam. We make different by landing you directly
                        to the locals and fall in line with Vietnamese culture from the moment you set your foot print in Vietnam. 
                    </p>
                    <div id="button-header-right" className="button">
                        <NavLink exact to = '/tour-package'>
                            Get Started
                            <img alt=" " src={arrow} />
                        </NavLink>
                    </div>
                </div>

                <div className="header-left">
                    <div>
                        <img alt="banner" src={banner} />
                    </div>
                    <div className="topLayer">
                        <div className="popUpPlayButton">
                            
                                <span onClick={()=>{
                                setPlay(true)
                            }}></span>
                            
                            
                        </div>

                        <div>
                            <h5>Watch an intro video</h5>
                            <p>Take only memories, leave only footprints.</p>
                        </div>
                    </div>
                    
                </div>

            </div>
            { play && <div>
                        <div className="outer-iframe-video"></div>
                        <div className="container-iframe-video">
                            <div className="wrapper-hold-iframe-video">
                                <div className="video-content">
                                    <button onClick={()=> setPlay(false)}>x</button>
                                    {play && 
                                    <iframe  
                                        src={url}
                                        frameborder="0"
                                        allowfullscreen
                                        title="amazing vietnam landviews"
                                    ></iframe>  
                                
                                }
                                </div>

                            </div>

                        </div>
                    </div>}
        </div>
    )
}
export default HeaderH;