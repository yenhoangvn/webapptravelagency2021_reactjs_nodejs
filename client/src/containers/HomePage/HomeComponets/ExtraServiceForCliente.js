import React from 'react';
import { NavLink } from 'react-router-dom';
import Blog1 from '../image/blog1.png';
import Blog2 from '../image/blog2.png';
import Blog3 from '../image/blog3.png';
import {FaCalendarAlt} from 'react-icons/fa';
import{FaHeart} from 'react-icons/fa';
import {FaComment} from 'react-icons/fa';
function ExtraService(){
    return(
        <div id="travelBlog" className="section-blogHomePage">
            <div className="container">
                <div className="travel-blog">
                    <div className="title-blog">
                        <h1>Travel Blog</h1>
                        <p></p>
                    </div>
                    <div className="single-travel-blog">
                        <div className="image-blog">
                            <img alt="blog-1" src={Blog1} />
                        </div>
                        <div className="blog-meta">
                            <NavLink exact to ="/blog-travel">
                                <div>
                                    <FaCalendarAlt />
                                    <p>20th Jun 2021</p>
                                </div>
                                <div>
                                    <FaHeart />
                                    <p>15</p>
                                </div>
                                <div>
                                    <FaComment />
                                    <p>05</p>
                                </div>
                            </NavLink>
                        </div>
                        <div className='blog-infos'>
                            <h5>
                                <NavLink exact to="/blog-travel"> 20 best family things to do in VietNam in 2021</NavLink>
                            </h5>
                            <p>
                            It’s coming up to the busiest family time in Vietnam so we have devoted the May Newsletter to things we know the whole family will enjoy at different locations. 
                            </p>
                        </div>
                        <div>

                        </div>
                    </div>
                    <div className="single-travel-blog">
                        <div className="image-blog">
                            <img alt="blog-2" src={Blog2} />
                        </div>
                        <div className="blog-meta">
                            <NavLink exact to ="/blog-travel">
                                <div>
                                    <FaCalendarAlt />
                                    <p>20th Jun 2021</p>
                                </div>
                                <div>
                                    <FaHeart />
                                    <p>15</p>
                                </div>
                                <div>
                                    <FaComment />
                                    <p>05</p>
                                </div>
                            </NavLink>
                        </div>
                        <div className='blog-infos'>
                            <h5>
                                <NavLink exact to="/blog-travel"> 20 best family things to do in VietNam in 2021</NavLink>
                            </h5>
                            <p>
                            It’s coming up to the busiest family time in Vietnam so we have devoted the May Newsletter to things we know the whole family will enjoy at different locations.
                            </p>
                        </div>

                    </div>
                    <div className="single-travel-blog">
                        <div className="image-blog">
                            <img alt="blog-3" src={Blog3} />
                        </div>
                        <div className="blog-meta">
                            <NavLink exact to ="/blog-travel">
                                <div>
                                    <FaCalendarAlt />
                                    <p>20th Jun 2021</p>
                                </div>
                                <div>
                                    <FaHeart />
                                    <p>15</p>
                                </div>
                                <div>
                                    <FaComment />
                                    <p>05</p>
                                </div>
                            </NavLink>
                        </div>
                        <div className='blog-infos'>
                            <h5>
                                <NavLink exact to="/blog-travel"> 20 best family things to do in VietNam in 2021</NavLink>
                            </h5>
                            <p>
                            It’s coming up to the busiest family time in Vietnam so we have devoted the May Newsletter to things we know the whole family will enjoy at different locations. 
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            {/* <div className="back-ground-change"></div> */}
           
            
        </div>
    )
}
export default ExtraService;