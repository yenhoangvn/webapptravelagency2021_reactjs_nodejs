import React from 'react';
import arrow from '../image/download.webp';
import axios from '../../../utility/config';

function PackageSearching(){

    const sendEmail = (event) =>{
        event.preventDefault();
        const nameInput = event.target.elements.name;
		const emailInput = event.target.elements.email;
        const destinationInput = event.target.elements.destination;
        const durationInput = event.target.elements.duration;
        const quantityInput = event.target.elements.quantity;
        const otherIssuesInput = event.target.elements.otherIssues;
        const subject = 'Tour query';
        const data = {
            name: nameInput.value,
            email: emailInput.value,
            subject : subject,
            message : `Tour Plan to  ${destinationInput.value} for ${durationInput.value} days, for ${quantityInput.value} people. Other issues: ${otherIssuesInput.value}`
            
        }
  
        axios.post('/emails/send_email', data)
		.then((response) => {
			nameInput.value = ""
			emailInput.value = ""
			destinationInput.value = ""
            durationInput.value =""
            quantityInput.value = ""
            otherIssuesInput.value =""
			alert("Your message has been sent, thanks!")

		})
		.catch(function (error) {
			console.log(error);
		})
		console.log("--SeNd!--")
    }
    return(
        <section id="contactUs" className="package-search-section">
            <div className="container">
                <div className="package-search-wrapper">
                    <div className="package-title">
                        <h1>Package Query & Design your own custom tour </h1>
                        <p>Please send us a custom tour enquiry to tell us your travel plans so that we can assist you. You will find that our efficiency, personal touch and high level of service will make your time in Asia an amazing experience you will never forget. We look forward to receiving your travel request.</p>
                        <p>3 steps to book a trip with us: </p>
                        <p>Step1: Fill out the search form</p>
                        <p>Step2: Choose your favorite tour</p>
                        <p>Step3: Submit form and we will contact you within 24 hours</p>
                    </div>
                    <div className="package-search-form">
                        <h2>Package Query</h2>
                        <form onSubmit={sendEmail}>
                            <div>
                                <input name="name" placeholder="Name" type="text" />
                            </div>
                            <div>
                                <input name="email" placeholder="Email" type="text" />
                            </div>
                            <div>
                                <input name="destination" placeholder="Destination" type="text" />
                            </div>
                            {/* <div>
                                <input placeholder="Depart Date" type="date" />
                            </div> */}
                            
                            <div>
                                <input name="duration" placeholder="Duration" type="Number" />
                            </div>
                            <div>
                                <input name="quantity" placeholder="Number of People" type="number" />
                            </div>
                            {/* <div>
                                <input placeholder="Children" type="number" />
                            </div> */}
                            <div>
                            <input name="otherIssues" placeholder="Other Issues" type="text" />
                            </div>
                            <div id="form-submit" className="button">
                                <button onSubmit={sendEmail}>
                                    Send Tour Query
                                    <img alt=" " src={arrow} />
                                </button>
                                
                            </div>
                        </form>

                    </div>

                </div>

            </div>

        </section>
    )
}
export default PackageSearching;