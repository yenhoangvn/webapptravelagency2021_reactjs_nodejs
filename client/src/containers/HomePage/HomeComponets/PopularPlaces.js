import React from 'react';
import image1 from '../image/popu.png';
import image2 from '../image/popu2.png';
import image3 from '../image/popu3.png';
import {FaPlane} from 'react-icons/fa';
import {FaRoute} from 'react-icons/fa';
import {FaFortAwesome} from 'react-icons/fa';
import {FaLandmark} from 'react-icons/fa';

function PopularPlaces(){
    // const style = {backgroundImage: `url(${image1})`} 
    return(
        <div className="container">
            <div className="pop-container">
                <div className="left-content">
                    <img src={image1} alt="image_popular_places_in-VietNam" />
                    <img src={image2} alt="image_popular_places_in-VietNam" />  
                    <img src={image3} alt="image_popular_places_in-VietNam" />
                </div>

                <div className="right-content">
                    <div>
                        <h1>Stunning places in VietNam</h1>
                        <p>Find a beautiful place inside the nature
                        and refresh yourself over there;
                        how well you repose will mostly determine how far you can go in this universe!
                        </p>
                    </div>
                    
                        <div className="single-counter">
                            <FaPlane />
                            <h2>The North</h2>
                            <p>50 places</p>
                        </div>
                        <div className="single-counter">
                            <FaRoute />
                            <h2>The South</h2>
                            <p>50 places</p>
                        </div>
                        <div className="single-counter">
                            <FaFortAwesome />
                            <h2>The Middle</h2>
                            <p>50 places</p>
                        </div>
                        <div className="single-counter">
                            <FaLandmark />
                            <h2>The Islands</h2>
                            <p>50 places</p>
                        </div>
                </div>
            </div>

        </div>
    )
}

export default PopularPlaces;