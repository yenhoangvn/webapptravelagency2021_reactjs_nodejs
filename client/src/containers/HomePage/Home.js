import React,{Suspense, useEffect} from 'react';
import HeaderH from './HomeComponets/HeaderH';
import PopularPlaces from './HomeComponets/PopularPlaces';
import BestTourPlan from './HomeComponets/BestTourPlan';
import ExtraService from './HomeComponets/ExtraServiceForCliente';
import PackageSearching from './HomeComponets/PackageSearch';
import NavBar from '../../components/NavBar/Navbar';
import CustomerReview from '../TourPackages/components-tourpackages/CustomerReview';
import OurService from '../TourPackages/components-tourpackages/YouMayAlsoLike';

function Home(props){
    const {isLoggedIn,userName} = props;
    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])
    return(
        <div>
            <Suspense fallback={<h1>Loading...</h1>}>
                <NavBar userName={userName} isLoggedIn={isLoggedIn}/>
                <HeaderH />
                <PopularPlaces />
                <BestTourPlan />
                <ExtraService />
                <PackageSearching />
                <CustomerReview />
                <OurService />
                {/* <SubscribeNewsletter /> */}
            </Suspense>
            
        </div>
    )
}
export default Home;