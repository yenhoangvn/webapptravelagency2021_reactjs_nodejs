import React, {useState} from 'react';
import { useEffect } from 'react';
import back1 from './images/amazingVietNam_buddha.jpeg';
import back2 from './images/tour2.jpeg';
import back3 from './images/back4.jpeg';
import {Link, NavLink} from 'react-router-dom';
import arrow from '../../HomePage/image/download.webp';
import {BsCaretRightFill} from 'react-icons/bs';
import {BsCaretLeftFill} from 'react-icons/bs';
import { themeColorState } from '../../../utility/State';
import { useSetRecoilState, useRecoilValue } from 'recoil';

function HeaderParallax({title}){
    const themeColor = useRecoilValue(themeColorState);
    const setThemeColor = useSetRecoilState(themeColorState);
    const images = [back3,back2,back1];
    const color = ['#3077CB','#94c1d1','#427497'];
    const [currentSlide, setCurrentSlide] = useState(0);
    const [activeStyle,setActiveStyle] = useState({backgroundImage: `url(${images[currentSlide]})`});
  
    useEffect(()=>{
        setActiveStyle({backgroundImage: `url(${images[currentSlide]})`})
        setThemeColor(color[currentSlide]);
    },[currentSlide])
    // useEffect(()=>{
    //     const autoSlide = setInterval(()=>{
    //       currentSlide === images.length-1 ?
    //       setCurrentSlide(0):setCurrentSlide(currentSlide+1)
    //     }, 3000);
    //   return () => clearInterval(autoSlide);
    //   },[currentSlide]);
    const previous = () =>{
        if(currentSlide>0){
            setCurrentSlide(currentSlide-1);
            setActiveStyle({backgroundImage: `url(${images[currentSlide]})`});
            setThemeColor(color[currentSlide]);
        }else{
            setCurrentSlide(images.length-1);
            setActiveStyle({backgroundImage: `url(${images[currentSlide]})`})
            setThemeColor(color[currentSlide]);
        }
    }
    console.log('current slide=====>',currentSlide);
    const next = () =>{
        if(currentSlide === images.length-1){
            setCurrentSlide(0);
            setActiveStyle({backgroundImage: `url(${images[currentSlide]})`});
            setThemeColor(color[currentSlide]);
        }else{
            setCurrentSlide(currentSlide+1);
            setActiveStyle({backgroundImage: `url(${images[currentSlide]})`});
            setThemeColor(color[currentSlide]);
        }
    }
    const borderStyle = {border:`1px solid ${themeColor}`}
    const textStyle = {color:'#fff'};
    const arrowStyle = {filter: 'invert(1)'};
    const slideControllerStyle =
    {
        color:'#C1C5C6',
        fontSize:'40px'

    }
    return(
        <div>
            <div style={activeStyle} className="header-parallax">
                <div className="overlay"></div>
                    <div className="title-package">
                        <div className="slide-controller-flex">
                            <div onClick={previous}>
                               <Link style={slideControllerStyle}>
                               <BsCaretLeftFill />
                               </Link>
                            </div>
                            <div>
                                <h1 style = {themeColor==='#3077CB' ? textStyle :null }>Live To Travel</h1>
                                <div style={borderStyle} className="button">
                                    <NavLink style = {themeColor==='#3077CB' ? textStyle :null } exact to = '/'>
                                        {`Home --> ${title}`}
                                        <img alt="arrow" style = {themeColor==='#3077CB' ? arrowStyle :null } src={arrow} />
                                    </NavLink>
                                </div>

                            </div>
                           <div onClick={next}>
                               <Link style={slideControllerStyle}>
                               <BsCaretRightFill/>
                               </Link>

                            </div>
                        
                    
                    </div>

                </div>

            </div>

        </div>
    )
}
export default HeaderParallax;