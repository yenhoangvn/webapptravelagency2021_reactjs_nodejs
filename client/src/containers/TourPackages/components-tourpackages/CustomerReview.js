import React from 'react';
import review1 from './images/review.png';

function CustomerReview(){
    return(
        <section id="testimonial" className="testimonial-section">
            <div className="container">
                <div className="title-intro-testimonial">
                    <h1>What customers say about us</h1>
                </div>
                <div className="main-testimonial">
                    <div>
                        <img alt="review" src={review1}/>
                    </div>
                    <div className="review-content">
                        <p>
                            “Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique dolorem quisquam laudantium, incidunt id laborum, tempora aliquid labore minus. Nemo maxime, veniam! Fugiat odio nam eveniet ipsam atque, corrupti porro”
                        </p>
                        <p>-
                            <em>Carlos Molins</em>,  
                            <a href="#">Traveller</a>                    
                        </p>
                    </div>

                </div>

            </div>

        </section>
    )
}
export default CustomerReview;