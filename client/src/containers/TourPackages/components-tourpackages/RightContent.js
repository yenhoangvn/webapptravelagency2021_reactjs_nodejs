import React, { useState, useEffect } from 'react';
import {FiSearch} from "react-icons/fi";
import CoreTourShop from './CoreTourShop'
import {  useRecoilValue } from 'recoil';
import { themeColorState } from '../../../utility/State';


function RightContent({fullData, clickedSubCate, priceRange, userId,isLoggedIn}){

    const themeColor = useRecoilValue(themeColorState);
    const [searchInput, setSearchInput] = useState()
    const [productDB, setProductDB] = useState(fullData.productDB);
    const [sortType, setSortType] = useState('sorting');
   
    useEffect(()=>{
        console.log('sort type',sortType)
    },[sortType]);

    useEffect(()=>{
        if(clickedSubCate){sortProductDB(clickedSubCate);}
    },[clickedSubCate]);

    useEffect(()=>{
        if(priceRange){sortProductDB(priceRange);}
    },[priceRange])

    const sortProductDB =(type)=>{
        console.log('type======>',type)
        if(type === 'sorting' || type === ''){
            setProductDB(fullData.productDB);
           
        }else if(type === 'bestTourPlan'){
            const filterProduct = fullData.productDB.filter(ele=>ele.bestSeller);
            setProductDB(filterProduct);

        }else if(type === 'ascendingPrice'){
            const sortingProduct1 = fullData.productDB.sort((a,b)=>a.price-b.price)
            setProductDB(sortingProduct1);
           
        }else if(type === 'descendingPrice'){
            const sortingProduct2 = fullData.productDB.sort((a,b)=>b.price-a.price);
            setProductDB(sortingProduct2);
      
        }else if (type === 'aToZ'){
            const sortingProduct4 = fullData.productDB.sort((a,b)=> a.product.localeCompare(b.product));
            setProductDB(sortingProduct4);
        }else if(type === 'zToA'){
            const sortingProduct4 = fullData.productDB.sort((a,b)=> b.product.localeCompare(a.product));
            setProductDB(sortingProduct4);

        }else if(typeof(type) === 'object'){
            // price range
            const result3 = fullData.productDB.filter(ele=> ele.price>=priceRange.inputLeft && ele.price <= priceRange.inputRight);
            setProductDB(result3);
            console.log('result===>',result3)
        }else{
            const result = fullData.productDB.filter(ele=>ele.product.includes(type));
            const result2 = fullData.productDB.filter(ele=>ele.subCategoryName===type);
            result.length!==0 ? setProductDB(result) : setProductDB(result2) 
        }
        setSortType(type);
    }
   
    const borderStyle = {borderBottom:`1.5px solid ${themeColor}`};

    if(!fullData){
        return(<p>Loading.....</p>)
    }else{
        return(
            <div className="right-grid-content-main-product">
                <div className="search-sort-bar">
                    <div style={borderStyle} className="sorting">
                        <select onChange={(e) => sortProductDB(e.target.value)}>
                            <option value="sorting">Sorting</option>
                            <option value="bestTourPlan">Best Tour Plan</option>
                            <option value="ascendingPrice">Price ascending</option>
                            <option value="descendingPrice">Price descending</option>
                            <option value="aToZ">A to Z</option>
                            <option value="zToA">Z to A</option>
                        </select>
                    </div>
                    {/* <div style={borderStyle} className="show-option">
                        <select>
                            <option>List</option>
                            <option>Feature</option>
                            <option>Icon</option>
                        </select>
    
                    </div> */}
                    <div className="search-bar">
                        <form
                        onSubmit = {(e)=>{
                            e.preventDefault();
                            sortProductDB(searchInput);
                            setSearchInput('');
                        }}
                        onChange={(e) =>{
                            setSearchInput(e.target.value);
                            sortProductDB(searchInput);
                        }} >
                            <input style={borderStyle} name="search-bar" value={searchInput} placeholder="Search Tour..." />
                            <FiSearch />

                        </form>
                        
                    </div>
                    

                </div>
                <CoreTourShop userId={userId} isLoggedIn={isLoggedIn} description={fullData.description} productDB={productDB} />
            </div>
        )

    }
   
}
export default RightContent;