import React, { useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import closeButton from './images/download.webp';
import {FaAngleDoubleRight,FaHandHoldingUsd,FaHourglassHalf,FaMapPin} from 'react-icons/fa';
import {FaStickyNote,FaPaperclip,FaTimesCircle,FaCheckCircle,FaAngleRight,FaAngleLeft} from 'react-icons/fa';
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { cartState } from '../../../utility/State';

function SingleTour({alert,description,chosedPTour,isLoggedIn}){

    const [hidden, setHidden] = useState(false);
    const descriptionForOneProduct = description.find(ele=>ele.productId===chosedPTour._id);
    const images = descriptionForOneProduct.images;
    const [currentSlide, setCurrentSlide] = useState(0);
    const style = {
        border: "2px solid greenyellow"
    }
    
    useEffect(()=>{
        alert(hidden);
    },[hidden]);

    useEffect(()=>{
        console.log('restSlider==>',currentSlide);
    },[currentSlide]);

    const clicked = ()=>{
        setHidden(true); 
    }

    const previous = () =>{
        if(currentSlide>0){
            setCurrentSlide(currentSlide-1);
        }else{
            setCurrentSlide(images.length-1);
        }
    }

    const next = () =>{
        if(currentSlide === images.length-1){
            setCurrentSlide(0);
        }else{
            setCurrentSlide(currentSlide+1);
        }
    }
    
    const [alertShowHidden,setAlertShowHidden] = useState([false,false,false]);
    const [styleShowHidden] = useState([
        {display:'none'},{display:'none'},{display:'none'}
    ])
    const [styleShow] = useState([
        {display:'block'},{display:'block'},{display:'block'}
    ])
    
    
 
    const[alertStyle,setAlertStyle] = useState({display:'none'})
    const [quantityTour,setQuantity] = useState(0);
    const [departDate,setDepartDate] = useState('');
    const [confirmation, setConfirmation] = useState({display:'none'});
    
    useEffect(()=>{
        console.log('quantity add onChange=====>',quantityTour)
    },[quantityTour]);
   
    const getInputProductInfo = ({target}) =>{
        let {name,value} = target;
        if(name ==='quantity'){
            setQuantity(Number(value));
        }else if(name ==='departDate'){
            setDepartDate(value);
        }        
    }
    const setCart = useSetRecoilState(cartState);
    const cart = useRecoilValue(cartState);
    const addToCart = (e,prod,quantity,date)=>{
        e.preventDefault();
        if(isLoggedIn){
            let tempCart = cart;
            let index = tempCart.findIndex(ele=>ele._id === prod._id);
            if(index === -1){
                let prodWithQuantity = {...prod,quantity:quantity,departDate:date};
                setCart([...tempCart, prodWithQuantity]);
            }else{
                let oldQuantity = Number(cart[index].quantity);
                let newQuantity = oldQuantity+quantity;
                let newTempCart = tempCart.map((element,i) => i === index ? {...element, quantity : newQuantity, departDate:date} : element);
                setCart(newTempCart);
            }
            setConfirmation({display:'block'});
        }else{
            setAlertStyle({display:'block'});
        }
    }

    return(
        <div className="wrapper-modal">
            <div className="mask"></div>
                <div id="container-modal" className="container">
                
                    <div id="quick-view-pop-up">
                        <button onClick={clicked} className="close-button">
                            <img src={closeButton} alt="CLOSE"/>
                        </button>
                        <div className="carousel-flex-container">
                            <div className="carouse-left-content">
                                <div className="left-outer-grid">
                                    <div className="mini-carousel-slider">
                                        {descriptionForOneProduct.images.map((ele,i)=>{
                                            return(
                                                <div style={i===currentSlide ? style : null} key={i} className="mini-carousel">
                                                    <img alt="mini carousel" onClick={
                                                        (e)=>{
                                                            setCurrentSlide(e.target.id);
                                                        }
                                                    } id={i} src={ele} />
                                                </div>
                                            )
                                        })}
                                    </div>
                                    
                                    <div className="active-carousel-showing">
                                        <div className="slide-controller">
                                            <button onClick={previous}><FaAngleLeft /></button>
                                            <button onClick={next}><FaAngleRight /></button>
                                        </div>
                                        <img alt="active carousel" src={currentSlide ? images[currentSlide] : images[0] }/>
                                    </div>

                                </div>
                            </div>

                            <div className="pop-up-right-content">
                                <h4>{chosedPTour.product}</h4>
                                <div className="tour-info">
                                    <span><FaHandHoldingUsd /> Price: €{chosedPTour.price}/Single Adult</span>
                                    <span><FaHourglassHalf />{`Duration: ${chosedPTour.duration}`}</span>
                                    <span><FaMapPin />{`Destination: ${chosedPTour.destination}`}</span>
                                </div>
                                
                                <div className="tour-brief">
                                    <h5><FaStickyNote />Tour Brief</h5>
                                    <FaAngleDoubleRight className="show-off" onClick={()=>{
                                        !alertShowHidden[0] ? setAlertShowHidden([true,false,false]) : setAlertShowHidden([false,false,false])
                                    }} />
                                    <div id="hidden-div-tourBrief" style={!alertShowHidden[0] ? styleShowHidden[0] : styleShow[0] } >
                                        {descriptionForOneProduct.tourBrief.map((item,i)=>{
                                            return(
                                                <p key={i}>{item}</p>
                                            )
                                        })}
                                    </div>
                                    
                                </div>
                                <div className="services-include-exclude">
                                    
                                    <div className="service-include">
                                        <h5><FaPaperclip/>Services Includes</h5>
                                        <FaAngleDoubleRight  className="show-off" onClick={()=>{
                                            !alertShowHidden[1] ? setAlertShowHidden([false,true,false]) : setAlertShowHidden([false,false,false])
                                        }} />
                                        <div style={!alertShowHidden[1] ? styleShowHidden[1] : styleShow[1]}>
                                            {descriptionForOneProduct.servicesInclude.map((item,i)=>{
                                                return(
                                                    <p key={i}>{item}</p>
                                                )
                                            })}
                                        </div>
                                        
                                    </div>
                                    <div className="service-include">
                                        <h5><FaTimesCircle/>Services Excludes</h5>
                                        <FaAngleDoubleRight className="show-off" onClick={()=>{
                                            !alertShowHidden[2] ? setAlertShowHidden([false,false,true]) : setAlertShowHidden([false,false,false])
                                        }} />
                                        <div style={!alertShowHidden[2] ? styleShowHidden[2] : styleShow[2]}>
                                            {descriptionForOneProduct.servicesExclude.map((item,i)=>{
                                                return(
                                                    <p key={i}>{item}</p>
                                                )
                                            })}
                                        </div>
                                        
                                    </div>
                                
                                    
                                </div>
                                <div className="querry-tour-booking">
                                    <form onChange={getInputProductInfo} onSubmit={(e)=>addToCart(e,chosedPTour,quantityTour,departDate)}>
                                        <div className="form-input-query">
                                        
                                            <label>Depart</label>
                                            <div className="input-outer-booking">
                                                <input required={true} name="departDate" className="depart-date" type="date" placeholder="depart date" />
                                            </div>
                                
                                        </div>

                                        <div className="quantity-controller">
                                            <div className="form-input-query">
                                                <label>Quantity</label>
                                                <div className="outer-quantity-controller">
                                                    <div onClick={()=> setQuantity(quantityTour -1)} className="substract">-</div>
                                                    
                                                    <input name="quantityTour" className="quantity" type="number" value={quantityTour} />
                                                    
                                                    {/* <div onClick={()=> test(quantityTour)} className="substract">+</div> */}
                                                    <div onClick={()=> setQuantity(quantityTour +1)} className="substract">+</div>
                                                </div>
                                            </div>
                                        </div>
                                        <button className="button-add-to-card">Add To Cart</button>
                                        <div className="alert-register" style={alertStyle}>please sign in to continue buying! Thank you! 
                                        <NavLink exact to ="/login">SignIn Now!</NavLink>
                                        </div>
                                        
                                    </form>
                                </div>
                                

                            </div>
                        </div>
                        

                    
                    </div>
                </div>

                <div style={confirmation} className="success-confirmation">
                    <div className="modal-confirm-overlay">
                        <div className="iconCheck">
                            <FaCheckCircle/>
                        </div>
                        <h4>{chosedPTour.product}</h4>
                        <p>is added to cart</p>
                        <button onClick={()=>setConfirmation({display:'none'})}>OK</button>
                    </div>
                </div>
           

        </div>
        
    )
}
export default SingleTour;