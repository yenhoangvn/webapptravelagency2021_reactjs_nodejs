import React, { useState, useEffect, Suspense } from 'react';
import LeftContent from './LeftContent';
import RightContent from './RightContent';
import axios from '../../../utility/config';

function Shop({userId,isLoggedIn,userName}){

    const [fullData, setFull] = useState();
    const[dataDone, setDataDone] = useState(false);

    useEffect(()=>{
        getFull();
    },[dataDone]);

    useEffect(() => {
        window.scrollTo(0, 0)
      }, [])

    const getFull = async () => {
        let url1 = '/products/';
        let url2 = '/categories/';
        let url3 = '/subCategories/';
        let url4 = '/descriptions/';
        try {
            const res1 = await axios.get(url1);
            const res2 = await axios.get(url2);
            const res3 = await axios.get(url3);
            const res4 = await axios.get(url4);
            
            let subCategory1 = [];
            let subCategory2 = [];
            res3.data.forEach(ele=>{
                ele.categoryId === res2.data[0]._id ? subCategory1.push(ele) :
                subCategory2.push(ele)
            })
            
            let fullData = {
                productDB: res1.data,
                categoryDB: res2.data,
                subCategoryDB: [subCategory1,subCategory2],
                description: res4.data
            };
            
            console.log("fullData after call=====>",fullData);
            setDataDone(true)
            setFull(fullData);  
        } catch (error) {
            console.log(error);     
        }
    }
    
    const [clickedSubCate,setClickedSubCate] = useState();
    const [priceRange, setPriceRange] = useState();

    const getData =(data)=>{
        setClickedSubCate(data);
        
    }

    const getPriceRange = (data) =>{
        console.log('price range from left content',data);
        setPriceRange(data);
    }

    useEffect(()=>{
        console.log('clickedSubCate from shop parent',clickedSubCate);
        
    },[clickedSubCate])

    useEffect(()=>{
        console.log('priceRange from shop parent',priceRange);
    },[priceRange])
    
    if(!fullData){
        return <p>loading...</p>
    }else{
        return(
            <div className="section-main-shop-tour-package">
                <Suspense fallback={<h1>Loading...</h1>}>
                    <div className="container">
                        <div className="grid-outer">
                            <LeftContent getPriceRange={getPriceRange} getData={getData} fullData={fullData} />
                            <RightContent userId={userId} isLoggedIn={isLoggedIn} userName={userName} priceRange={priceRange} clickedSubCate={clickedSubCate} fullData={fullData} />
                        </div>
    
                    </div>
                </Suspense>
            </div>
        )
    }
}

export default Shop;



