import React, {useState, useEffect, useRef} from 'react';
import { FiChevronUp } from 'react-icons/fi';
import {Link} from 'react-router-dom';
import { themeColorState } from '../../../utility/State';
import {  useRecoilValue } from 'recoil';


function LeftContent({fullData, getData, getPriceRange}){
    const themeColor = useRecoilValue(themeColorState);
    const [clickedSub,setClickedSub] = useState();
    const [inputValue, setInput] = useState({
        inputLeft: '0',
        inputRight: '2000'
    });
    console.log('input range price',inputValue);
    // need to put getData() in useEffect to pass info immediately to parent!!! Important!!!
    useEffect(()=>{
        getData(clickedSub);
    },[clickedSub])
    useEffect(()=>{
        getPriceRange(inputValue);
    },[inputValue])
    const myRefs = useRef([]);
    
    //loop to set ref of each div
    if(fullData){
        myRefs.current = fullData.categoryDB.map((ele, i) => myRefs.current[i] ?? React.createRef());

    }
   
    const handleChange =({target})=>{
        const {name,value} = target;
        setInput((prev)=>({...prev,[name]:value}));
    }

    const handleClick = ({target}) =>{

        const index = Number(target.id);
        if(myRefs.current[index].current.style.display === 'none'){
            myRefs.current[index].current.style.display = 'flex';
        }else{
            myRefs.current[index].current.style.display='none';
        }     
    }
    const styleBackGround = {backgroundColor:themeColor};
    const styleText = {color:themeColor};
    if(!fullData){
        return <p>loading...</p>
    }else{
        return(
            <div className="left-side-container">
                <div className="browser-categories">
                    <div style={styleBackGround} className="title-widget">
                        <h5>Browser Category</h5>
                    </div>
                    <div className="browser-cate-content">
                        {fullData.categoryDB.map((ele,i)=>{
                            return(
                                <div key={i}>
                                    <div className="toggle-up-down-subCategory">
                                        <span style={styleText} name="categoryName" id={i} onClick={handleClick} className="container-check-category">{ele.category}
                                        </span>
                                     
                                        <FiChevronUp name="icon" id={i} onClick={handleClick} />
                                        
                                    </div>
                                    
                                    <div ref={myRefs.current[i]} className="subcategory-container">
                                        {fullData.subCategoryDB[i].map((ele,index)=>{
                                            return(
                                                <Link key={index+10}>
                                                    <span onClick={(e)=>{
                                                        setClickedSub(e.target.innerHTML);
                                                    
                                                    }} 
                                                    className="container-check-subcategory">{ele.subCategory}
                                                    </span>
                                                </Link>
                                            )
                                        })}  
                                    </div>
                                </div>
                            )
                        })}      
                    </div>
                </div>

                <div className="browser-categories">
                        <div style={styleBackGround} className="title-widget">
                           <h5> Price Filters</h5>
                        </div>
                        <div id="price-filter" className="browser-cate-content">
                            <div className="slidecontainer">
                                <h2 style={styleText} >Price Range</h2>
                                <form onSubmit={(e)=>{
                                    e.preventDefault();

                                }}>
                                    <input onChange={handleChange} type="range" min="0" max="1000" value={inputValue.inputLeft} className="slider" name="inputLeft" />
                                    <input onChange={handleChange} type="range" min="1000" max="2000" value={inputValue.inputRight} className="slider-right" name="inputRight" />
                                    <div>
                                        <p>{ inputValue.inputLeft }€</p>
                                        <p> {inputValue.inputRight}€</p>
                                    </div>
                                    
                                    <button>
                                        <Link style={styleText} >
                                        Filter
                                        </Link>
                                    </button>
                                </form>
                            </div>
                        
                       </div>
                      

                    </div>
    
            </div>
        )

    }
    
   
}
export default LeftContent;