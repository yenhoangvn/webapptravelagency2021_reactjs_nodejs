import React from 'react';
import service2 from './images/tourGuide.png';
import service3 from './images/passport.png';
import service4 from './images/honeymoon.png';
import service5 from './images/map.png';

function YouMayAlsoLike(){
    return(
        <div>
            <div className="container">
                <div className="section-services-outer">
                <div className="intro">
                    {/* <span className="subHeading">Stay Tunned</span> */}
                    <h1> Our Services</h1>
                    {/* <p>Our Services</p> */}
                    {/* <p>Take only memories, leave only foot prints</p> */}
                </div>
                
                    <div className="col1-visa-service">
                        <div>
                            <span className="flaticon">
                                <img alt="flaticon-visa-service" src={service3}/>
                            </span>
                        </div>
                        <div>
                            <h3>Visa services</h3>
                            <p>
                            A small river named Duden flows by their place and supplies it with the necessary regelialia.
                            </p>
                        </div>
                    </div>

                    <div className="col1-visa-service">
                        <div>
                            <span className="flaticon">
                            <img alt="flaticon-travel-arrangement" src={service4}/>
                            </span>
                            
                        </div>
                        <div>
                            <h3>Travel arrangement</h3>
                            <p>
                            A small river named Duden flows by their place and supplies it with the necessary regelialia.
                            </p>
                        </div>

                    </div>

                    <div className="col1-visa-service">
                        <div>
                            <span className="flaticon">
                            <img alt="flaticon-private-guide" src={service2}/>
                            </span>
                        </div>
                        <div>
                            <h3>Private Guide</h3>
                            <p>
                            A small river named Duden flows by their place and supplies it with the necessary regelialia.
                            </p>
                        </div>

                    </div>

                    <div className="col1-visa-service">
                        <div>
                            <span className="flaticon">
                            <img alt="flaticon-location management" src={service5}/>
                            </span>
                        </div>
                        <div>
                            <h3>Location Management</h3>
                            <p>
                            A small river named Duden flows by their place and supplies it with the necessary regelialia.
                            </p>
                        </div>

                    </div>

          

                </div>
                

            </div>
           

        </div>
    )
}
export default YouMayAlsoLike;