import React, { useState, useEffect } from 'react';
import Thumbnail1 from './images/thumbnail.webp';
import { FiHeart, FiShoppingCart } from "react-icons/fi";
import {FaStar} from 'react-icons/fa';
import {Link} from 'react-router-dom';
import SingleTour from './SingleTour';
import {FaHeart} from 'react-icons/fa';
import { useSetRecoilState, useRecoilValue } from 'recoil';
import { cartState } from '../../../utility/State';
import { wishListState } from '../../../utility/State';

function CoreTourShop({productDB,description, userId, isLoggedIn}){
 
    const [visible,setVisible] = useState(false);
    const [chosedPTour,setChosedTour] = useState()
    
    useEffect(()=>{
        console.log(chosedPTour);
    },[chosedPTour])

    const hiddenFromChild = (alert)=>{
        alert ? setVisible(false) : setVisible(true)
    }

    const clickedPopUp = ({target})=>{
        setVisible(true);
        // window.scrollTo(0,0);
        const chosed = target.id;
        const tour = productDB.find(ele=>ele._id === chosed)
        setChosedTour(tour);
    }    
    
    const setCart = useSetRecoilState(cartState);
    const cart = useRecoilValue(cartState);
    const setWishList = useSetRecoilState(wishListState);
    const wishList = useRecoilValue(wishListState);


    let addToCart = (prod) => {
        let tempCart = cart;
        let index = cart.findIndex(ele=>ele._id === prod._id);
        if(index === -1){
            let prodWithQuantity = {...prod,quantity:1};
            setCart([...tempCart, prodWithQuantity]);
        }else{
            let oldQuantity = Number(cart[index].quantity);
            let newQuantity = oldQuantity+1;
            let newTempCart = tempCart.map((element,i) => i === index ? {...element, quantity : newQuantity} : element);
            setCart(newTempCart);
        }
    }
  
    let addToWishList = (prod,i) => {
        let tempWishList = wishList;
        if(!wishList.some(ele=>ele._id === prod._id)){
            setWishList([...tempWishList, prod]);
        }else{
            let newtempWishList = tempWishList.filter(ele=>ele._id !== prod._id);
            setWishList(newtempWishList);
        }
    }

    if(productDB.length===0){
        return(
            <h1>Sorry! This tour does not exist!</h1>
        )
    }else{
        return(
        <div className="main-content-product-wrapper">
                
                    <div className="best-seller-tour">
                        <div className="best-seller-outer">
                            <div className="best-seller-title">
                                {/* <h1>Best Tour Plan</h1> */}
                            </div>
                            {
                                productDB.map((ele,i)=>{
                                    return(
                                        <div key={i}>
                                        <div className="single-tour-plan">
                                            <div className="img">
                                               {ele.onSale ?
                                                <div className="vr">
                                                    <span>Sale</span>
                                                </div> : null}
                                                {
                                                    ele.images ?
                                                    <Link>
                                                        <img onClick={clickedPopUp} id={ele._id} alt={ele.product} src={process.env.PUBLIC_URL + ele.images} />
                                                    </Link> :
                                                        <img alt="thumbnail singer tour" src={Thumbnail1} />
                                                }   
                                            </div>
                                            <div className="text">
                                                <h4 className="price">
                                                    {ele.salePrice ?<span className="old-price">€{ele.salePrice}</span> : null}
                                                    €{ele.price}
                                                </h4>
                                                <div className="titleLikeButton">
                                                    <span>{`${ele.duration} tour`}</span>
                                                    <div>
                                                        {/* {wishList.map((item, index) => item._id === ele._id?  <div>{index}<div/>)} */}
                                                        <Link onClick={()=>addToWishList(ele,i)}>{!wishList.some(item=>item._id===ele._id)? <FiHeart /> : <FaHeart/>}</Link>
                                                        <Link id={i} onClick={()=>addToCart(ele)} ><FiShoppingCart id={i} /></Link>
                                                    </div>
                                                </div>
                                                
                                                <h3>
                                                    {<Link>{ele.product}</Link>}
                                                </h3>
                                                <div className="review-outer">
                                                    <div id="star" className="review-star">
                                                        <FaStar />
                                                        <FaStar />
                                                        <FaStar />
                                                        <FaStar />
                                                        <FaStar />
                                                    </div>
                                                    <p>{(120)}</p>
                                                </div>
                                            </div>
                                       </div>

                                       {visible && <SingleTour isLoggedIn={isLoggedIn} chosedPTour={chosedPTour} description={description} alert={hiddenFromChild} />}
                                       </div>

                                    )
                                })
                            }
                           
    
                        </div>
    
                    </div>
                    
                </div>
    )
}
}
export default CoreTourShop;