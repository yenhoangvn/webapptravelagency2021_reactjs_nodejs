import React from 'react';
import HeaderParallax from './components-tourpackages/HeaderScrollParallax';
import NavbarShop from './components-tourpackages/NavbarShop';
import MainShop from './components-tourpackages/Shop';
import YouMayAlsoLike from './components-tourpackages/YouMayAlsoLike';
import CustomerReview from './components-tourpackages/CustomerReview';


function Product({userId,userName,isLoggedIn}){
 
    return (
        <section>
            <NavbarShop userId={userId} isLoggedIn={isLoggedIn}/>
            <HeaderParallax title={'Package'} />
            <MainShop userId={userId} isLoggedIn={isLoggedIn} userName={userName} />
            <YouMayAlsoLike />
            <CustomerReview />
        </section>
    )
}
export default Product;
