## N'bi in Vietnam - Travel Agency web app
<!-- blank line -->
----
<!-- blank line -->
### DESCRIPTION
This is a **MERN** (MongoDB, Express, React.js, Node) stack e-commerce website. As _admin_, you can log in and complete **CRUD** (create, read, update and delete) operations for both products and blog posts. As _customer_, you can register, login, add products to wishlist or shopping-cart (increase and decrease quantity, delete on product from the cart). Conduct a fully automated payment using Stripe. Also, client can query tour infos via a form (with nodemailer)!
<!-- blank line -->
----
<!-- blank line -->
### VIEW DEMO AT
Deploy on Heroku: <https://deploytestproject.herokuapp.com/>
<!-- blank line -->
----
<!-- blank line -->
### HOW TO RUN IT
#### From client folder
```
1. cd client
2. npm install
3. npm start
```
#### From server folder
```
1. cd server
2. npm install
3. nodemon
```
<!-- blank line -->
----
<!-- blank line -->
### HOW TO CHECK PAYMENT FUNCTIONALITY
Apply card number: 4242 4242 4242 4242 Expiration date: any future date CVC code: any code ZIP: any number

