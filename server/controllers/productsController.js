const Products = require('../models/productsModel');
const Categories = require('../models/categoriesModel');
const SubCategories = require('../models/SubCategoriesModel');

const getAllProducts = async (req, res) => {
    try{
        const products = await Products.find({});
        res.send(products);
    }
    catch(e){
        res.send({e})
    }

}

// FIND BY CATEGORY NAME
const findProduct = async (req ,res) => {
    let {category} = req.params
    console.log(req.params)
    try{
        const product = await Products.find({categoryName:category});
        res.send(product);
    }
    catch(e){
        res.send({e})
    }

}
    
const createProduct = async (req, res) => {
    let { product,salePrice, price, duration, destination, bestSeller, onSale, subCategoryName, categoryName, images} = req.body;
    try{
        const choseCate = await Categories.findOne({category:categoryName});
        const choseSubCate = await SubCategories.findOne({subCategory:subCategoryName})
        const created = await Products.create({
            product,
            categoryId: choseCate._id,
            categoryName,
            subCategoryName,
            subCategoryId: choseSubCate._id,
            salePrice,
            price,
            duration,
            destination,
            bestSeller,
            onSale,
            images
        });

        res.send(created)
    }
    catch(e){
        res.send({e})
    }

}

const deleteProduct = async (req, res) => {
    let { productId } = req.params;
    try{
        const removed = await Products.deleteOne({ _id: productId });
        res.send({removed});
    }
    catch(error){
        res.send({error});
    };

}

// UPDATE 

const updateProduct = async (req, res) => {
    let { newProduct } = req.body;
    let {productId} = req.params;
    try{
        const product = await Products.findOne({_id:productId})
        const updated = await Products.updateOne(
            { product },{ product:newProduct }
         );
        res.send({updated});
    }
    catch(error){
        res.send({error});
    };
}

module.exports = {
    getAllProducts,
    createProduct,
    deleteProduct,
    findProduct,
    updateProduct
}
