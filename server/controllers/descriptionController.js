const Descriptions = require('../models/descriptionModel');
const Products = require('../models/productsModel');
// const SubCategories = require('../models/SubCategoriesModel');

const findAllDescription = async (req, res) => {
    try{
        const description = await Descriptions.find({});
        res.send(description );
    }
    catch(e){
        res.send({e})
    }

}

const findDescription = async (req, res) => {
    let {productId} = req.params;
    try{
        const description = await Descriptions.findById({productId});
        res.send(description );
    }
    catch(e){
        res.send({e})
    }

}


const createDescription = async (req, res) => {
    let {tourBrief,productId,servicesInclude,servicesExclude,images} = req.body;
    try{
        const description = await Posts.createOne({
            tourBrief,productId,servicesInclude,servicesExclude,images,productName
        });
        res.send(description);
    }
    catch(e){
        res.send({e})
    }
}



module.exports = {
    findAllDescription,
    findDescription,
    createDescription
}