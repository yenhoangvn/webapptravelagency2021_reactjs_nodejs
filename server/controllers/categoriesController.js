const Categories = require('../models/categoriesModel');

const getAllCategory = async (req,res) => {
    try{
        const categories = await Categories.find({});
        res.send(categories);
    }
    catch(e){
        res.send({e})
    }

}

const createCategory = async (req,res) => {
    let { category } = req.body;
    try{
        const created = await Categories.create({category});
        res.send(created)
    }
    catch(e){
        res.send({e})
    }

}

const deleteCategory = async (req,res) => {
    let { categoryId } = req.params;
    try{
        const removed = await Categories.deleteOne({_id:categoryId });
        res.send({removed});
    }
    catch(error){
        res.send({error});
    };

}

const updateCategory = async (req,res) => {
    let { categoryId } = req.params;
    let {newCategory} = req.body;
    try{
        const updated = await Categories.updateOne(
            {category}, {category:newCategory}
        );
        res.send({updated});
    }
    catch(error){
        res.send({error});
    };

}

module.exports = {
    getAllCategory,
    createCategory,
    deleteCategory,
    updateCategory
}
