const SubCategories = require('../models/SubCategoriesModel');
const Categories = require('../models/categoriesModel');

const getAllSubCategories = async (req, res) => {
    try{
        const subCategories = await SubCategories.find({});
        res.send(subCategories);
    }
    catch(e){
        res.send({e})
    }

}

const createSubCategory = async (req, res) => {
    let { subCategory, categoryName } = req.body;
    try{
        const choseCate = await Categories.findOne({category:categoryName});
        const created = await SubCategories.create({
            subCategory, 
            categoryName,
            categoryId:choseCate._id
        });
        res.send(created)
    }
    catch(e){
        res.send({e})
    }

}

const deleteSubCategory = async (req, res) => {
    let { subCategoryId } = req.params;
    try{
        const removed = await SubCategories.deleteOne({_id:subCategoryId });
        res.send({removed});
    }
    catch(error){
        res.send({error});
    };

}

const updateSubCategory = async (req, res) => {
    let { newSubCategory } = req.body;
    let {subCategoryId} = req.params;
    try{
        const subCategory = await SubCategories.findOne({_id:subCategoryId})
        const updated = await SubCategories.updateOne(
            { subCategory },{ subCategory:newSubCategory }
         );
        res.send({updated});
    }
    catch(error){
        res.send({error});
    };
}

module.exports = {
    getAllSubCategories,
    createSubCategory,
    deleteSubCategory,
    updateSubCategory
}
