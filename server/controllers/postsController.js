const Posts = require('../models/postsModel');
// const Categories = require('../models/categoriesModel');
// const SubCategories = require('../models/SubCategoriesModel');

const findAllPost = async (req, res) => {
    try{
        const posts = await Posts.find({});
        res.send(posts);
    }
    catch(e){
        res.send({e})
    }

}
const findPost = async (req, res) => {
    let {postId} = req.params;
    try{
        const posts = await Posts.findById({_id:postId});
        res.send(posts);
    }
    catch(e){
        res.send({e})
    }

}

const createPost = async (req, res) => {
    let {title, content, author,categoryName,categoryId} = req.body;
    try{
        const posts = await Posts.createOne({
            title,content, author, categoryName,categoryId
        });
        res.send(posts);
    }
    catch(e){
        res.send({e})
    }

}


module.exports = {
    findAllPost,
    findPost,
    createPost
}