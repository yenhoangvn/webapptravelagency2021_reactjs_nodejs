const stripe = require("stripe")(process.env.STRIPE_SECRET_KEY);

const create_payment = async (req, res) => {
  try{
    const {userId,email,name,subTotal,pmId} = req.body;
    let customer = await stripe.customers.create({
      email: email,
      name: name,
      metadata: {
        userId: userId
      },
    })
    
    console.log('customer here===>',customer.id)
    const paymentIntent = await stripe.paymentIntents.create({
      amount: subTotal*100,
      currency: 'eur',
      payment_method_types: ['card'],
      payment_method: pmId,
      customer: customer.id,
      confirm: true //
    });
    console.log('paymentIntent here===>',paymentIntent.id)
    return res.send({ ok: true, paymentIntentId: paymentIntent.id });

  }catch(e){
    return res.send({ ok: false, message: e.raw.message });

  }
}
module.exports = {
  create_payment
};

