const Comments = require('../models/commentsModel');
// const Categories = require('../models/categoriesModel');
// const SubCategories = require('../models/SubCategoriesModel');

const findPostComment = async (req, res) => {
    let{postId} = req.params
    try{
        const comments = await Comments.find({postId:postId});
        res.send(comments);
    }
    catch(e){
        res.send({e})
    }

}

const createComment = async (req, res) => {
    let {userName, content, date, postId} = req.body;
    console.log(userName)
    try{
        const comments = await Comments.create({
            userName, content, date, postId
        });
        res.send(comments);

    }
    catch(e){
        res.send({e})
    }

}


module.exports = {
    findPostComment,
    createComment
}