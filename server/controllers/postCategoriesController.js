const PostCategories = require('../models/postCategoriesModel');
// const Categories = require('../models/categoriesModel');
// const SubCategories = require('../models/SubCategoriesModel');

const findAllPostCategory = async (req, res) => {
    try{
        const postCates = await PostCategories.find({});
        res.send(postCates);
    }
    catch(e){
        res.send({e})
    }

}
const findPostCategory = async (req, res) => {
    let {postCateId} = req.params;
    try{
        const postCate = await PostCategories.findById({_id:postCateId});
        res.send(postCate);
    }
    catch(e){
        res.send({e})
    }

}

const createPostCategory = async (req, res) => {
    let {category} = req.body;
    try{
        const postCate = await Posts.createOne({
            category
        });
        res.send(postCate);
    }
    catch(e){
        res.send({e})
    }

}


module.exports = {
    findAllPostCategory,
    findPostCategory,
    createPostCategory
}