const express = require('express');
const router = express.Router();
const controller = require('../controllers/subCategoriesController');

router.get('/', controller.getAllSubCategories);
router.post('/create', controller.createSubCategory);
router.delete('/delete/:subCategoryId', controller.deleteSubCategory);
router.post('/update/:subCategoryId', controller.updateSubCategory);
module.exports = router;
