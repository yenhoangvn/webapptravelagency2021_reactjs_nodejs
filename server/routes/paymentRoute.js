const router = require("express").Router();
const controller = require("../controllers/paymentController");

router.post("/create_payment", controller.create_payment);


module.exports = router;