const router = require('express').Router();
const controller = require('../controllers/descriptionController');
router.get('/', controller.findAllDescription);
router.get('/:productId', controller.findDescription);
router.post('/create/:productId', controller.createDescription);
// router.post('/update/:productId', controller.updateDescription);
// router.delete('/remove/productId', controller.deleteDescription);
module.exports = router;