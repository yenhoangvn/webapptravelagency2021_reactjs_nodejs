const express = require('express');
const router = express.Router();
const controller = require('../controllers/productsController');

router.get('/', controller.getAllProducts);
router.get('/:category', controller.findProduct);
router.post('/create', controller.createProduct);
router.delete('/delete/:productId', controller.deleteProduct);
router.post('/update/:productId', controller.updateProduct);

module.exports = router;
