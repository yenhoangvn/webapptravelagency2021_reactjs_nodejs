const router = require('express').Router();
const controller = require('../controllers/postCategoriesController');
router.get('/', controller.findAllPostCategory);
router.get('/:postId', controller.findPostCategory);
router.post('/create', controller.createPostCategory);
// router.post('/update', controller.updatePost);
// router.delete('/remove', controller.deletePost);
module.exports = router;