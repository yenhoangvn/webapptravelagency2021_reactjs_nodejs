const express = require('express');
const router = express.Router();
const controller = require('../controllers/categoriesController');

router.get('/', controller.getAllCategory);
router.post('/create', controller.createCategory);
router.delete('/delete/:categoryId', controller.deleteCategory);
router.post('/update/:categoryId', controller.updateCategory)

module.exports = router;
