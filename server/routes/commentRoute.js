
const router = require('express').Router();
const controller = require('../controllers/commentsController');
router.get('/:postId', controller.findPostComment);
// router.get('/:postId', controller.findPost);
router.post('/create', controller.createComment);
// router.post('/update', controller.updatePost);
// router.delete('/remove', controller.deletePost);
module.exports = router;