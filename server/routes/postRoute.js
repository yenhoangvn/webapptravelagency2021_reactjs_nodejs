const router = require('express').Router();
const controller = require('../controllers/postsController');
router.get('/', controller.findAllPost);
router.get('/:postId', controller.findPost);
router.post('/create', controller.createPost);
// router.post('/update', controller.updatePost);
// router.delete('/remove', controller.deletePost);
module.exports = router;