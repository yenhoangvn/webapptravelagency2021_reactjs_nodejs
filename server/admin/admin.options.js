const AdminBro = require('admin-bro');
const AdminBroMongoose = require('admin-bro-mongoose');
const Categories = require('../models/categoriesModel'); 
const SubCategories = require('../models/SubCategoriesModel'); 
const Products = require('../models/productsModel'); 
const Descriptions = require('../models/descriptionModel');
// const ShoppingCart = require('../models/cartModel');
const UsersAdmin = require('./resource_options/users.admin'); 
const Posts = require('../models/postsModel');
const PostCategories = require('../models/postCategoriesModel');

AdminBro.registerAdapter(AdminBroMongoose);

const options = {
	resources: [ Categories, SubCategories, Products, Descriptions, UsersAdmin, Posts, PostCategories ] // the models must be included in this array
};

module.exports = options;
