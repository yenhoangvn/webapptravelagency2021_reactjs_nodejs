const mongoose = require('mongoose');
const postSchema = new mongoose.Schema({
	title: { type: String, unique: true, required: true },
	content: { type: String, required: true },
    date: {type: Date, required: true, default: Date.now},
    likes: {type: Array, default: []},
    author: {type: String, required: true},
    categoryId: {type: String, required: true},
    categoryName: {type: String, required: true}
});
module.exports = mongoose.model('posts', postSchema);