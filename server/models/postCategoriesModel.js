const mongoose = require('mongoose');
const postCategorySchema = new mongoose.Schema({
	category: { type: String, unique: true, required: true }
});
module.exports = mongoose.model('postCategory', postCategorySchema);