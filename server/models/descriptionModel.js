const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const descriptionSchema = new Schema({
    tourBrief:{type: Array},
    servicesInclude: {type: Array},
    servicesExclude:{type:Array},
    productId: {type: String, required: true},
    productName:{type:String},
    images:{type:Array},
    destinationBrief:{type:String}
})

module.exports = mongoose.model('descriptions', descriptionSchema);