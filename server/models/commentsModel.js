const mongoose = require('mongoose');
const commentSchema = new mongoose.Schema({
	userName: { type: String, unique: true, required: true },
	content: { type: String, required: true },
    date: {type: String, required: true},
    postId: {type: String, required: true},
    reported: {type: Array, default:[]}
});
module.exports = mongoose.model('comments', commentSchema);