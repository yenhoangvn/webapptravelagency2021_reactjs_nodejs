const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productsSchema = new Schema({
	product: {
		type: String,
		required: true,
		unique: true
	},
	price: {
		type: Number,
		required: true
    },
	duration:{
		type: String,
		required: true
	},
	destination:{
		type: String,
		required: true
	},
	bestSeller:{
		type:Boolean,
		required:false
	},
	onSale:{
		type: Boolean,
		default: false
	},
	salePrice:{
		type:Number
	},
    categoryId: {
		type: String,
		required:true
    },
	categoryName: {
		type: String,
		required: true
    },
	subCategoryName: {
		type: String,
		required: true
    },
	subCategoryId: {
		type: String,
		required: true
    },
	images:{type:String}

});

module.exports = mongoose.model('products', productsSchema);
