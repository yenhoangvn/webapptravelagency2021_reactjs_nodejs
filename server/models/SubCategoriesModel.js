const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const subCategoriesSchema = new Schema({
    subCategory: {
      type: String,
      required: true,
      unique: true
    },
    categoryId: {
      type: String,
      required: true
    },
    categoryName: {
      type: String,
      required: true
      }
})
module.exports =  mongoose.model('subCategories', subCategoriesSchema);
