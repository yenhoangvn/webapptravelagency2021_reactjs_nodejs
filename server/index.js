const express = require('express'),
      app = express(),
      mongoose = require('mongoose'),
      cors = require('cors')
      AdminBro = require('admin-bro'),
      options = require('./admin/admin.options'),
      buildAdminRouter = require('./admin/admin.router')

//* ============   ADMIN BRO   ================
const admin = new AdminBro(options);
const router = buildAdminRouter(admin);
app.use(admin.options.rootPath, router);



// require('dotenv').config();
require("dotenv").config({ path: "./.env" });

const port = process.env.PORT || 5003;

// // TO DEPLOY

// if(process.env.NODE_ENV === 'production'){
const path = require('path');
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/build')));

// }

app.use(cors())
app.use(express.urlencoded({extended:true}))
app.use(express.json())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
    res.header('Access-Control-Allow-Credentials', true);
    next();
});



async function connecting(){
    try {
        await mongoose.connect('mongodb+srv://ruby:123qwe@coffeedb.qrsvw.mongodb.net/coffeeDB?retryWrites=true&w=majority', { useUnifiedTopology: true , useNewUrlParser: true })
        console.log('Connected to the DB')
    } catch ( error ) {
        console.log('ERROR: Seems like your DB is not running, please start it up !!!');
    }
}
connecting()
mongoose.set('useCreateIndex', true);
app.use('/users', require('./routes/userRoute'));
app.use('/categories',require('./routes/categoriesRoute'));
app.use('/subCategories',require('./routes/subCategoriesRoute'));
app.use('/products',require('./routes/productsRoute'));
// app.use('/blog',require('./routes/postRoute'));
// app.use('/cart',require('./routes/shoppingCartRoute'));
app.use('/descriptions',require('./routes/descriptionRoute'));
app.listen(port, () => console.log(`server listening on port ${port}`));
app.use('/emails', require('./routes/emailsRoute'));
app.options('/sendEmail', cors());
app.use("/payment", require("./routes/paymentRoute"));
// app.use("/orders", require("./routes/orderRoute"));
app.use("/post",require("./routes/postRoute"))
app.use("/postCategories",require('./routes/postCategoryRoute'))
app.use("/comment",require('./routes/commentRoute'))

app.get('/*', function (req, res) {
    res.sendFile(path.join(__dirname, '../client/build', 'index.html'));
});